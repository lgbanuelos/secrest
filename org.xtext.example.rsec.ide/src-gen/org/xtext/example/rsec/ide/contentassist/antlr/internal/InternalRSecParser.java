package org.xtext.example.rsec.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.rsec.services.RSecGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRSecParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'package'", "'datatype'", "'import'", "'.*'", "'.'", "'role'", "'resource'", "'end'", "'on'", "'view'", "'data'", "'actions'", "'{'", "'->'", "'}'", "','", "'states'", "'('", "')'", "':'", "'with'", "'['", "']'", "'state'", "'=>'", "'many'", "'roles'", "'rendered'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRSecParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRSecParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRSecParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRSec.g"; }


    	private RSecGrammarAccess grammarAccess;

    	public void setGrammarAccess(RSecGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleResourceSpecification"
    // InternalRSec.g:53:1: entryRuleResourceSpecification : ruleResourceSpecification EOF ;
    public final void entryRuleResourceSpecification() throws RecognitionException {
        try {
            // InternalRSec.g:54:1: ( ruleResourceSpecification EOF )
            // InternalRSec.g:55:1: ruleResourceSpecification EOF
            {
             before(grammarAccess.getResourceSpecificationRule()); 
            pushFollow(FOLLOW_1);
            ruleResourceSpecification();

            state._fsp--;

             after(grammarAccess.getResourceSpecificationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleResourceSpecification"


    // $ANTLR start "ruleResourceSpecification"
    // InternalRSec.g:62:1: ruleResourceSpecification : ( ( rule__ResourceSpecification__Group__0 ) ) ;
    public final void ruleResourceSpecification() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:66:2: ( ( ( rule__ResourceSpecification__Group__0 ) ) )
            // InternalRSec.g:67:2: ( ( rule__ResourceSpecification__Group__0 ) )
            {
            // InternalRSec.g:67:2: ( ( rule__ResourceSpecification__Group__0 ) )
            // InternalRSec.g:68:3: ( rule__ResourceSpecification__Group__0 )
            {
             before(grammarAccess.getResourceSpecificationAccess().getGroup()); 
            // InternalRSec.g:69:3: ( rule__ResourceSpecification__Group__0 )
            // InternalRSec.g:69:4: rule__ResourceSpecification__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ResourceSpecification__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getResourceSpecificationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleResourceSpecification"


    // $ANTLR start "entryRuleType"
    // InternalRSec.g:78:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalRSec.g:79:1: ( ruleType EOF )
            // InternalRSec.g:80:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalRSec.g:87:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:91:2: ( ( ( rule__Type__Alternatives ) ) )
            // InternalRSec.g:92:2: ( ( rule__Type__Alternatives ) )
            {
            // InternalRSec.g:92:2: ( ( rule__Type__Alternatives ) )
            // InternalRSec.g:93:3: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // InternalRSec.g:94:3: ( rule__Type__Alternatives )
            // InternalRSec.g:94:4: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleDataType"
    // InternalRSec.g:103:1: entryRuleDataType : ruleDataType EOF ;
    public final void entryRuleDataType() throws RecognitionException {
        try {
            // InternalRSec.g:104:1: ( ruleDataType EOF )
            // InternalRSec.g:105:1: ruleDataType EOF
            {
             before(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleDataType();

            state._fsp--;

             after(grammarAccess.getDataTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalRSec.g:112:1: ruleDataType : ( ( rule__DataType__Group__0 ) ) ;
    public final void ruleDataType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:116:2: ( ( ( rule__DataType__Group__0 ) ) )
            // InternalRSec.g:117:2: ( ( rule__DataType__Group__0 ) )
            {
            // InternalRSec.g:117:2: ( ( rule__DataType__Group__0 ) )
            // InternalRSec.g:118:3: ( rule__DataType__Group__0 )
            {
             before(grammarAccess.getDataTypeAccess().getGroup()); 
            // InternalRSec.g:119:3: ( rule__DataType__Group__0 )
            // InternalRSec.g:119:4: rule__DataType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleImport"
    // InternalRSec.g:128:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalRSec.g:129:1: ( ruleImport EOF )
            // InternalRSec.g:130:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalRSec.g:137:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:141:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalRSec.g:142:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalRSec.g:142:2: ( ( rule__Import__Group__0 ) )
            // InternalRSec.g:143:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalRSec.g:144:3: ( rule__Import__Group__0 )
            // InternalRSec.g:144:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalRSec.g:153:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // InternalRSec.g:154:1: ( ruleQualifiedNameWithWildcard EOF )
            // InternalRSec.g:155:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalRSec.g:162:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:166:2: ( ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) )
            // InternalRSec.g:167:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            {
            // InternalRSec.g:167:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            // InternalRSec.g:168:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 
            // InternalRSec.g:169:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            // InternalRSec.g:169:4: rule__QualifiedNameWithWildcard__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalRSec.g:178:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalRSec.g:179:1: ( ruleQualifiedName EOF )
            // InternalRSec.g:180:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalRSec.g:187:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:191:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalRSec.g:192:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalRSec.g:192:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalRSec.g:193:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalRSec.g:194:3: ( rule__QualifiedName__Group__0 )
            // InternalRSec.g:194:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleRoles"
    // InternalRSec.g:203:1: entryRuleRoles : ruleRoles EOF ;
    public final void entryRuleRoles() throws RecognitionException {
        try {
            // InternalRSec.g:204:1: ( ruleRoles EOF )
            // InternalRSec.g:205:1: ruleRoles EOF
            {
             before(grammarAccess.getRolesRule()); 
            pushFollow(FOLLOW_1);
            ruleRoles();

            state._fsp--;

             after(grammarAccess.getRolesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRoles"


    // $ANTLR start "ruleRoles"
    // InternalRSec.g:212:1: ruleRoles : ( ( rule__Roles__Group__0 ) ) ;
    public final void ruleRoles() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:216:2: ( ( ( rule__Roles__Group__0 ) ) )
            // InternalRSec.g:217:2: ( ( rule__Roles__Group__0 ) )
            {
            // InternalRSec.g:217:2: ( ( rule__Roles__Group__0 ) )
            // InternalRSec.g:218:3: ( rule__Roles__Group__0 )
            {
             before(grammarAccess.getRolesAccess().getGroup()); 
            // InternalRSec.g:219:3: ( rule__Roles__Group__0 )
            // InternalRSec.g:219:4: rule__Roles__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Roles__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRolesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRoles"


    // $ANTLR start "entryRuleResourceType"
    // InternalRSec.g:228:1: entryRuleResourceType : ruleResourceType EOF ;
    public final void entryRuleResourceType() throws RecognitionException {
        try {
            // InternalRSec.g:229:1: ( ruleResourceType EOF )
            // InternalRSec.g:230:1: ruleResourceType EOF
            {
             before(grammarAccess.getResourceTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleResourceType();

            state._fsp--;

             after(grammarAccess.getResourceTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleResourceType"


    // $ANTLR start "ruleResourceType"
    // InternalRSec.g:237:1: ruleResourceType : ( ( rule__ResourceType__Group__0 ) ) ;
    public final void ruleResourceType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:241:2: ( ( ( rule__ResourceType__Group__0 ) ) )
            // InternalRSec.g:242:2: ( ( rule__ResourceType__Group__0 ) )
            {
            // InternalRSec.g:242:2: ( ( rule__ResourceType__Group__0 ) )
            // InternalRSec.g:243:3: ( rule__ResourceType__Group__0 )
            {
             before(grammarAccess.getResourceTypeAccess().getGroup()); 
            // InternalRSec.g:244:3: ( rule__ResourceType__Group__0 )
            // InternalRSec.g:244:4: rule__ResourceType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleResourceType"


    // $ANTLR start "entryRuleEvent"
    // InternalRSec.g:253:1: entryRuleEvent : ruleEvent EOF ;
    public final void entryRuleEvent() throws RecognitionException {
        try {
            // InternalRSec.g:254:1: ( ruleEvent EOF )
            // InternalRSec.g:255:1: ruleEvent EOF
            {
             before(grammarAccess.getEventRule()); 
            pushFollow(FOLLOW_1);
            ruleEvent();

            state._fsp--;

             after(grammarAccess.getEventRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalRSec.g:262:1: ruleEvent : ( ( rule__Event__Group__0 ) ) ;
    public final void ruleEvent() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:266:2: ( ( ( rule__Event__Group__0 ) ) )
            // InternalRSec.g:267:2: ( ( rule__Event__Group__0 ) )
            {
            // InternalRSec.g:267:2: ( ( rule__Event__Group__0 ) )
            // InternalRSec.g:268:3: ( rule__Event__Group__0 )
            {
             before(grammarAccess.getEventAccess().getGroup()); 
            // InternalRSec.g:269:3: ( rule__Event__Group__0 )
            // InternalRSec.g:269:4: rule__Event__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleProperty"
    // InternalRSec.g:278:1: entryRuleProperty : ruleProperty EOF ;
    public final void entryRuleProperty() throws RecognitionException {
        try {
            // InternalRSec.g:279:1: ( ruleProperty EOF )
            // InternalRSec.g:280:1: ruleProperty EOF
            {
             before(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getPropertyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalRSec.g:287:1: ruleProperty : ( ( rule__Property__Group__0 ) ) ;
    public final void ruleProperty() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:291:2: ( ( ( rule__Property__Group__0 ) ) )
            // InternalRSec.g:292:2: ( ( rule__Property__Group__0 ) )
            {
            // InternalRSec.g:292:2: ( ( rule__Property__Group__0 ) )
            // InternalRSec.g:293:3: ( rule__Property__Group__0 )
            {
             before(grammarAccess.getPropertyAccess().getGroup()); 
            // InternalRSec.g:294:3: ( rule__Property__Group__0 )
            // InternalRSec.g:294:4: rule__Property__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Property__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleState"
    // InternalRSec.g:303:1: entryRuleState : ruleState EOF ;
    public final void entryRuleState() throws RecognitionException {
        try {
            // InternalRSec.g:304:1: ( ruleState EOF )
            // InternalRSec.g:305:1: ruleState EOF
            {
             before(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            ruleState();

            state._fsp--;

             after(grammarAccess.getStateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalRSec.g:312:1: ruleState : ( ( rule__State__Group__0 ) ) ;
    public final void ruleState() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:316:2: ( ( ( rule__State__Group__0 ) ) )
            // InternalRSec.g:317:2: ( ( rule__State__Group__0 ) )
            {
            // InternalRSec.g:317:2: ( ( rule__State__Group__0 ) )
            // InternalRSec.g:318:3: ( rule__State__Group__0 )
            {
             before(grammarAccess.getStateAccess().getGroup()); 
            // InternalRSec.g:319:3: ( rule__State__Group__0 )
            // InternalRSec.g:319:4: rule__State__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalRSec.g:328:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // InternalRSec.g:329:1: ( ruleTransition EOF )
            // InternalRSec.g:330:1: ruleTransition EOF
            {
             before(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getTransitionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalRSec.g:337:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:341:2: ( ( ( rule__Transition__Group__0 ) ) )
            // InternalRSec.g:342:2: ( ( rule__Transition__Group__0 ) )
            {
            // InternalRSec.g:342:2: ( ( rule__Transition__Group__0 ) )
            // InternalRSec.g:343:3: ( rule__Transition__Group__0 )
            {
             before(grammarAccess.getTransitionAccess().getGroup()); 
            // InternalRSec.g:344:3: ( rule__Transition__Group__0 )
            // InternalRSec.g:344:4: rule__Transition__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "rule__Type__Alternatives"
    // InternalRSec.g:352:1: rule__Type__Alternatives : ( ( ruleDataType ) | ( ruleResourceType ) | ( ruleRoles ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:356:1: ( ( ruleDataType ) | ( ruleResourceType ) | ( ruleRoles ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt1=1;
                }
                break;
            case 17:
                {
                alt1=2;
                }
                break;
            case 16:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalRSec.g:357:2: ( ruleDataType )
                    {
                    // InternalRSec.g:357:2: ( ruleDataType )
                    // InternalRSec.g:358:3: ruleDataType
                    {
                     before(grammarAccess.getTypeAccess().getDataTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleDataType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getDataTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalRSec.g:363:2: ( ruleResourceType )
                    {
                    // InternalRSec.g:363:2: ( ruleResourceType )
                    // InternalRSec.g:364:3: ruleResourceType
                    {
                     before(grammarAccess.getTypeAccess().getResourceTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleResourceType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getResourceTypeParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalRSec.g:369:2: ( ruleRoles )
                    {
                    // InternalRSec.g:369:2: ( ruleRoles )
                    // InternalRSec.g:370:3: ruleRoles
                    {
                     before(grammarAccess.getTypeAccess().getRolesParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleRoles();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getRolesParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__ResourceSpecification__Group__0"
    // InternalRSec.g:379:1: rule__ResourceSpecification__Group__0 : rule__ResourceSpecification__Group__0__Impl rule__ResourceSpecification__Group__1 ;
    public final void rule__ResourceSpecification__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:383:1: ( rule__ResourceSpecification__Group__0__Impl rule__ResourceSpecification__Group__1 )
            // InternalRSec.g:384:2: rule__ResourceSpecification__Group__0__Impl rule__ResourceSpecification__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__ResourceSpecification__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceSpecification__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__0"


    // $ANTLR start "rule__ResourceSpecification__Group__0__Impl"
    // InternalRSec.g:391:1: rule__ResourceSpecification__Group__0__Impl : ( () ) ;
    public final void rule__ResourceSpecification__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:395:1: ( ( () ) )
            // InternalRSec.g:396:1: ( () )
            {
            // InternalRSec.g:396:1: ( () )
            // InternalRSec.g:397:2: ()
            {
             before(grammarAccess.getResourceSpecificationAccess().getResourceSpecificationAction_0()); 
            // InternalRSec.g:398:2: ()
            // InternalRSec.g:398:3: 
            {
            }

             after(grammarAccess.getResourceSpecificationAccess().getResourceSpecificationAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__0__Impl"


    // $ANTLR start "rule__ResourceSpecification__Group__1"
    // InternalRSec.g:406:1: rule__ResourceSpecification__Group__1 : rule__ResourceSpecification__Group__1__Impl rule__ResourceSpecification__Group__2 ;
    public final void rule__ResourceSpecification__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:410:1: ( rule__ResourceSpecification__Group__1__Impl rule__ResourceSpecification__Group__2 )
            // InternalRSec.g:411:2: rule__ResourceSpecification__Group__1__Impl rule__ResourceSpecification__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__ResourceSpecification__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceSpecification__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__1"


    // $ANTLR start "rule__ResourceSpecification__Group__1__Impl"
    // InternalRSec.g:418:1: rule__ResourceSpecification__Group__1__Impl : ( 'package' ) ;
    public final void rule__ResourceSpecification__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:422:1: ( ( 'package' ) )
            // InternalRSec.g:423:1: ( 'package' )
            {
            // InternalRSec.g:423:1: ( 'package' )
            // InternalRSec.g:424:2: 'package'
            {
             before(grammarAccess.getResourceSpecificationAccess().getPackageKeyword_1()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getResourceSpecificationAccess().getPackageKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__1__Impl"


    // $ANTLR start "rule__ResourceSpecification__Group__2"
    // InternalRSec.g:433:1: rule__ResourceSpecification__Group__2 : rule__ResourceSpecification__Group__2__Impl rule__ResourceSpecification__Group__3 ;
    public final void rule__ResourceSpecification__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:437:1: ( rule__ResourceSpecification__Group__2__Impl rule__ResourceSpecification__Group__3 )
            // InternalRSec.g:438:2: rule__ResourceSpecification__Group__2__Impl rule__ResourceSpecification__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__ResourceSpecification__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceSpecification__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__2"


    // $ANTLR start "rule__ResourceSpecification__Group__2__Impl"
    // InternalRSec.g:445:1: rule__ResourceSpecification__Group__2__Impl : ( ( rule__ResourceSpecification__PackageNameAssignment_2 ) ) ;
    public final void rule__ResourceSpecification__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:449:1: ( ( ( rule__ResourceSpecification__PackageNameAssignment_2 ) ) )
            // InternalRSec.g:450:1: ( ( rule__ResourceSpecification__PackageNameAssignment_2 ) )
            {
            // InternalRSec.g:450:1: ( ( rule__ResourceSpecification__PackageNameAssignment_2 ) )
            // InternalRSec.g:451:2: ( rule__ResourceSpecification__PackageNameAssignment_2 )
            {
             before(grammarAccess.getResourceSpecificationAccess().getPackageNameAssignment_2()); 
            // InternalRSec.g:452:2: ( rule__ResourceSpecification__PackageNameAssignment_2 )
            // InternalRSec.g:452:3: rule__ResourceSpecification__PackageNameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ResourceSpecification__PackageNameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getResourceSpecificationAccess().getPackageNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__2__Impl"


    // $ANTLR start "rule__ResourceSpecification__Group__3"
    // InternalRSec.g:460:1: rule__ResourceSpecification__Group__3 : rule__ResourceSpecification__Group__3__Impl rule__ResourceSpecification__Group__4 ;
    public final void rule__ResourceSpecification__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:464:1: ( rule__ResourceSpecification__Group__3__Impl rule__ResourceSpecification__Group__4 )
            // InternalRSec.g:465:2: rule__ResourceSpecification__Group__3__Impl rule__ResourceSpecification__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__ResourceSpecification__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceSpecification__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__3"


    // $ANTLR start "rule__ResourceSpecification__Group__3__Impl"
    // InternalRSec.g:472:1: rule__ResourceSpecification__Group__3__Impl : ( ( rule__ResourceSpecification__ImportsAssignment_3 )* ) ;
    public final void rule__ResourceSpecification__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:476:1: ( ( ( rule__ResourceSpecification__ImportsAssignment_3 )* ) )
            // InternalRSec.g:477:1: ( ( rule__ResourceSpecification__ImportsAssignment_3 )* )
            {
            // InternalRSec.g:477:1: ( ( rule__ResourceSpecification__ImportsAssignment_3 )* )
            // InternalRSec.g:478:2: ( rule__ResourceSpecification__ImportsAssignment_3 )*
            {
             before(grammarAccess.getResourceSpecificationAccess().getImportsAssignment_3()); 
            // InternalRSec.g:479:2: ( rule__ResourceSpecification__ImportsAssignment_3 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalRSec.g:479:3: rule__ResourceSpecification__ImportsAssignment_3
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__ResourceSpecification__ImportsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getResourceSpecificationAccess().getImportsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__3__Impl"


    // $ANTLR start "rule__ResourceSpecification__Group__4"
    // InternalRSec.g:487:1: rule__ResourceSpecification__Group__4 : rule__ResourceSpecification__Group__4__Impl ;
    public final void rule__ResourceSpecification__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:491:1: ( rule__ResourceSpecification__Group__4__Impl )
            // InternalRSec.g:492:2: rule__ResourceSpecification__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceSpecification__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__4"


    // $ANTLR start "rule__ResourceSpecification__Group__4__Impl"
    // InternalRSec.g:498:1: rule__ResourceSpecification__Group__4__Impl : ( ( rule__ResourceSpecification__ElementsAssignment_4 )* ) ;
    public final void rule__ResourceSpecification__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:502:1: ( ( ( rule__ResourceSpecification__ElementsAssignment_4 )* ) )
            // InternalRSec.g:503:1: ( ( rule__ResourceSpecification__ElementsAssignment_4 )* )
            {
            // InternalRSec.g:503:1: ( ( rule__ResourceSpecification__ElementsAssignment_4 )* )
            // InternalRSec.g:504:2: ( rule__ResourceSpecification__ElementsAssignment_4 )*
            {
             before(grammarAccess.getResourceSpecificationAccess().getElementsAssignment_4()); 
            // InternalRSec.g:505:2: ( rule__ResourceSpecification__ElementsAssignment_4 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==12||(LA3_0>=16 && LA3_0<=17)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalRSec.g:505:3: rule__ResourceSpecification__ElementsAssignment_4
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__ResourceSpecification__ElementsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getResourceSpecificationAccess().getElementsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__Group__4__Impl"


    // $ANTLR start "rule__DataType__Group__0"
    // InternalRSec.g:514:1: rule__DataType__Group__0 : rule__DataType__Group__0__Impl rule__DataType__Group__1 ;
    public final void rule__DataType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:518:1: ( rule__DataType__Group__0__Impl rule__DataType__Group__1 )
            // InternalRSec.g:519:2: rule__DataType__Group__0__Impl rule__DataType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__DataType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__0"


    // $ANTLR start "rule__DataType__Group__0__Impl"
    // InternalRSec.g:526:1: rule__DataType__Group__0__Impl : ( 'datatype' ) ;
    public final void rule__DataType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:530:1: ( ( 'datatype' ) )
            // InternalRSec.g:531:1: ( 'datatype' )
            {
            // InternalRSec.g:531:1: ( 'datatype' )
            // InternalRSec.g:532:2: 'datatype'
            {
             before(grammarAccess.getDataTypeAccess().getDatatypeKeyword_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDataTypeAccess().getDatatypeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__0__Impl"


    // $ANTLR start "rule__DataType__Group__1"
    // InternalRSec.g:541:1: rule__DataType__Group__1 : rule__DataType__Group__1__Impl ;
    public final void rule__DataType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:545:1: ( rule__DataType__Group__1__Impl )
            // InternalRSec.g:546:2: rule__DataType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__1"


    // $ANTLR start "rule__DataType__Group__1__Impl"
    // InternalRSec.g:552:1: rule__DataType__Group__1__Impl : ( ( rule__DataType__NameAssignment_1 ) ) ;
    public final void rule__DataType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:556:1: ( ( ( rule__DataType__NameAssignment_1 ) ) )
            // InternalRSec.g:557:1: ( ( rule__DataType__NameAssignment_1 ) )
            {
            // InternalRSec.g:557:1: ( ( rule__DataType__NameAssignment_1 ) )
            // InternalRSec.g:558:2: ( rule__DataType__NameAssignment_1 )
            {
             before(grammarAccess.getDataTypeAccess().getNameAssignment_1()); 
            // InternalRSec.g:559:2: ( rule__DataType__NameAssignment_1 )
            // InternalRSec.g:559:3: rule__DataType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DataType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__Group__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalRSec.g:568:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:572:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalRSec.g:573:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalRSec.g:580:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:584:1: ( ( 'import' ) )
            // InternalRSec.g:585:1: ( 'import' )
            {
            // InternalRSec.g:585:1: ( 'import' )
            // InternalRSec.g:586:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalRSec.g:595:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:599:1: ( rule__Import__Group__1__Impl )
            // InternalRSec.g:600:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalRSec.g:606:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:610:1: ( ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) )
            // InternalRSec.g:611:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            {
            // InternalRSec.g:611:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            // InternalRSec.g:612:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 
            // InternalRSec.g:613:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            // InternalRSec.g:613:3: rule__Import__ImportedNamespaceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportedNamespaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0"
    // InternalRSec.g:622:1: rule__QualifiedNameWithWildcard__Group__0 : rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 ;
    public final void rule__QualifiedNameWithWildcard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:626:1: ( rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 )
            // InternalRSec.g:627:2: rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__QualifiedNameWithWildcard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0__Impl"
    // InternalRSec.g:634:1: rule__QualifiedNameWithWildcard__Group__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:638:1: ( ( ruleQualifiedName ) )
            // InternalRSec.g:639:1: ( ruleQualifiedName )
            {
            // InternalRSec.g:639:1: ( ruleQualifiedName )
            // InternalRSec.g:640:2: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1"
    // InternalRSec.g:649:1: rule__QualifiedNameWithWildcard__Group__1 : rule__QualifiedNameWithWildcard__Group__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:653:1: ( rule__QualifiedNameWithWildcard__Group__1__Impl )
            // InternalRSec.g:654:2: rule__QualifiedNameWithWildcard__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1__Impl"
    // InternalRSec.g:660:1: rule__QualifiedNameWithWildcard__Group__1__Impl : ( ( '.*' )? ) ;
    public final void rule__QualifiedNameWithWildcard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:664:1: ( ( ( '.*' )? ) )
            // InternalRSec.g:665:1: ( ( '.*' )? )
            {
            // InternalRSec.g:665:1: ( ( '.*' )? )
            // InternalRSec.g:666:2: ( '.*' )?
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
            // InternalRSec.g:667:2: ( '.*' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalRSec.g:667:3: '.*'
                    {
                    match(input,14,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalRSec.g:676:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:680:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalRSec.g:681:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalRSec.g:688:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:692:1: ( ( RULE_ID ) )
            // InternalRSec.g:693:1: ( RULE_ID )
            {
            // InternalRSec.g:693:1: ( RULE_ID )
            // InternalRSec.g:694:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalRSec.g:703:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:707:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalRSec.g:708:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalRSec.g:714:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:718:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalRSec.g:719:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalRSec.g:719:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalRSec.g:720:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalRSec.g:721:2: ( rule__QualifiedName__Group_1__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==15) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalRSec.g:721:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalRSec.g:730:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:734:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalRSec.g:735:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_4);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalRSec.g:742:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:746:1: ( ( '.' ) )
            // InternalRSec.g:747:1: ( '.' )
            {
            // InternalRSec.g:747:1: ( '.' )
            // InternalRSec.g:748:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalRSec.g:757:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:761:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalRSec.g:762:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalRSec.g:768:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:772:1: ( ( RULE_ID ) )
            // InternalRSec.g:773:1: ( RULE_ID )
            {
            // InternalRSec.g:773:1: ( RULE_ID )
            // InternalRSec.g:774:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__Roles__Group__0"
    // InternalRSec.g:784:1: rule__Roles__Group__0 : rule__Roles__Group__0__Impl rule__Roles__Group__1 ;
    public final void rule__Roles__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:788:1: ( rule__Roles__Group__0__Impl rule__Roles__Group__1 )
            // InternalRSec.g:789:2: rule__Roles__Group__0__Impl rule__Roles__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Roles__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Roles__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Roles__Group__0"


    // $ANTLR start "rule__Roles__Group__0__Impl"
    // InternalRSec.g:796:1: rule__Roles__Group__0__Impl : ( 'role' ) ;
    public final void rule__Roles__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:800:1: ( ( 'role' ) )
            // InternalRSec.g:801:1: ( 'role' )
            {
            // InternalRSec.g:801:1: ( 'role' )
            // InternalRSec.g:802:2: 'role'
            {
             before(grammarAccess.getRolesAccess().getRoleKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getRolesAccess().getRoleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Roles__Group__0__Impl"


    // $ANTLR start "rule__Roles__Group__1"
    // InternalRSec.g:811:1: rule__Roles__Group__1 : rule__Roles__Group__1__Impl ;
    public final void rule__Roles__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:815:1: ( rule__Roles__Group__1__Impl )
            // InternalRSec.g:816:2: rule__Roles__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Roles__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Roles__Group__1"


    // $ANTLR start "rule__Roles__Group__1__Impl"
    // InternalRSec.g:822:1: rule__Roles__Group__1__Impl : ( ( rule__Roles__NameAssignment_1 ) ) ;
    public final void rule__Roles__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:826:1: ( ( ( rule__Roles__NameAssignment_1 ) ) )
            // InternalRSec.g:827:1: ( ( rule__Roles__NameAssignment_1 ) )
            {
            // InternalRSec.g:827:1: ( ( rule__Roles__NameAssignment_1 ) )
            // InternalRSec.g:828:2: ( rule__Roles__NameAssignment_1 )
            {
             before(grammarAccess.getRolesAccess().getNameAssignment_1()); 
            // InternalRSec.g:829:2: ( rule__Roles__NameAssignment_1 )
            // InternalRSec.g:829:3: rule__Roles__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Roles__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRolesAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Roles__Group__1__Impl"


    // $ANTLR start "rule__ResourceType__Group__0"
    // InternalRSec.g:838:1: rule__ResourceType__Group__0 : rule__ResourceType__Group__0__Impl rule__ResourceType__Group__1 ;
    public final void rule__ResourceType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:842:1: ( rule__ResourceType__Group__0__Impl rule__ResourceType__Group__1 )
            // InternalRSec.g:843:2: rule__ResourceType__Group__0__Impl rule__ResourceType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__ResourceType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__0"


    // $ANTLR start "rule__ResourceType__Group__0__Impl"
    // InternalRSec.g:850:1: rule__ResourceType__Group__0__Impl : ( 'resource' ) ;
    public final void rule__ResourceType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:854:1: ( ( 'resource' ) )
            // InternalRSec.g:855:1: ( 'resource' )
            {
            // InternalRSec.g:855:1: ( 'resource' )
            // InternalRSec.g:856:2: 'resource'
            {
             before(grammarAccess.getResourceTypeAccess().getResourceKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getResourceKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__0__Impl"


    // $ANTLR start "rule__ResourceType__Group__1"
    // InternalRSec.g:865:1: rule__ResourceType__Group__1 : rule__ResourceType__Group__1__Impl rule__ResourceType__Group__2 ;
    public final void rule__ResourceType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:869:1: ( rule__ResourceType__Group__1__Impl rule__ResourceType__Group__2 )
            // InternalRSec.g:870:2: rule__ResourceType__Group__1__Impl rule__ResourceType__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__ResourceType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__1"


    // $ANTLR start "rule__ResourceType__Group__1__Impl"
    // InternalRSec.g:877:1: rule__ResourceType__Group__1__Impl : ( ( rule__ResourceType__NameAssignment_1 ) ) ;
    public final void rule__ResourceType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:881:1: ( ( ( rule__ResourceType__NameAssignment_1 ) ) )
            // InternalRSec.g:882:1: ( ( rule__ResourceType__NameAssignment_1 ) )
            {
            // InternalRSec.g:882:1: ( ( rule__ResourceType__NameAssignment_1 ) )
            // InternalRSec.g:883:2: ( rule__ResourceType__NameAssignment_1 )
            {
             before(grammarAccess.getResourceTypeAccess().getNameAssignment_1()); 
            // InternalRSec.g:884:2: ( rule__ResourceType__NameAssignment_1 )
            // InternalRSec.g:884:3: rule__ResourceType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__1__Impl"


    // $ANTLR start "rule__ResourceType__Group__2"
    // InternalRSec.g:892:1: rule__ResourceType__Group__2 : rule__ResourceType__Group__2__Impl rule__ResourceType__Group__3 ;
    public final void rule__ResourceType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:896:1: ( rule__ResourceType__Group__2__Impl rule__ResourceType__Group__3 )
            // InternalRSec.g:897:2: rule__ResourceType__Group__2__Impl rule__ResourceType__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__ResourceType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__2"


    // $ANTLR start "rule__ResourceType__Group__2__Impl"
    // InternalRSec.g:904:1: rule__ResourceType__Group__2__Impl : ( ( rule__ResourceType__Group_2__0 )? ) ;
    public final void rule__ResourceType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:908:1: ( ( ( rule__ResourceType__Group_2__0 )? ) )
            // InternalRSec.g:909:1: ( ( rule__ResourceType__Group_2__0 )? )
            {
            // InternalRSec.g:909:1: ( ( rule__ResourceType__Group_2__0 )? )
            // InternalRSec.g:910:2: ( rule__ResourceType__Group_2__0 )?
            {
             before(grammarAccess.getResourceTypeAccess().getGroup_2()); 
            // InternalRSec.g:911:2: ( rule__ResourceType__Group_2__0 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==19) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalRSec.g:911:3: rule__ResourceType__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ResourceType__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getResourceTypeAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__2__Impl"


    // $ANTLR start "rule__ResourceType__Group__3"
    // InternalRSec.g:919:1: rule__ResourceType__Group__3 : rule__ResourceType__Group__3__Impl rule__ResourceType__Group__4 ;
    public final void rule__ResourceType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:923:1: ( rule__ResourceType__Group__3__Impl rule__ResourceType__Group__4 )
            // InternalRSec.g:924:2: rule__ResourceType__Group__3__Impl rule__ResourceType__Group__4
            {
            pushFollow(FOLLOW_11);
            rule__ResourceType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__3"


    // $ANTLR start "rule__ResourceType__Group__3__Impl"
    // InternalRSec.g:931:1: rule__ResourceType__Group__3__Impl : ( ( rule__ResourceType__Group_3__0 )? ) ;
    public final void rule__ResourceType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:935:1: ( ( ( rule__ResourceType__Group_3__0 )? ) )
            // InternalRSec.g:936:1: ( ( rule__ResourceType__Group_3__0 )? )
            {
            // InternalRSec.g:936:1: ( ( rule__ResourceType__Group_3__0 )? )
            // InternalRSec.g:937:2: ( rule__ResourceType__Group_3__0 )?
            {
             before(grammarAccess.getResourceTypeAccess().getGroup_3()); 
            // InternalRSec.g:938:2: ( rule__ResourceType__Group_3__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==20) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalRSec.g:938:3: rule__ResourceType__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ResourceType__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getResourceTypeAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__3__Impl"


    // $ANTLR start "rule__ResourceType__Group__4"
    // InternalRSec.g:946:1: rule__ResourceType__Group__4 : rule__ResourceType__Group__4__Impl rule__ResourceType__Group__5 ;
    public final void rule__ResourceType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:950:1: ( rule__ResourceType__Group__4__Impl rule__ResourceType__Group__5 )
            // InternalRSec.g:951:2: rule__ResourceType__Group__4__Impl rule__ResourceType__Group__5
            {
            pushFollow(FOLLOW_11);
            rule__ResourceType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__4"


    // $ANTLR start "rule__ResourceType__Group__4__Impl"
    // InternalRSec.g:958:1: rule__ResourceType__Group__4__Impl : ( ( rule__ResourceType__Group_4__0 )? ) ;
    public final void rule__ResourceType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:962:1: ( ( ( rule__ResourceType__Group_4__0 )? ) )
            // InternalRSec.g:963:1: ( ( rule__ResourceType__Group_4__0 )? )
            {
            // InternalRSec.g:963:1: ( ( rule__ResourceType__Group_4__0 )? )
            // InternalRSec.g:964:2: ( rule__ResourceType__Group_4__0 )?
            {
             before(grammarAccess.getResourceTypeAccess().getGroup_4()); 
            // InternalRSec.g:965:2: ( rule__ResourceType__Group_4__0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==21) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalRSec.g:965:3: rule__ResourceType__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ResourceType__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getResourceTypeAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__4__Impl"


    // $ANTLR start "rule__ResourceType__Group__5"
    // InternalRSec.g:973:1: rule__ResourceType__Group__5 : rule__ResourceType__Group__5__Impl rule__ResourceType__Group__6 ;
    public final void rule__ResourceType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:977:1: ( rule__ResourceType__Group__5__Impl rule__ResourceType__Group__6 )
            // InternalRSec.g:978:2: rule__ResourceType__Group__5__Impl rule__ResourceType__Group__6
            {
            pushFollow(FOLLOW_11);
            rule__ResourceType__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__5"


    // $ANTLR start "rule__ResourceType__Group__5__Impl"
    // InternalRSec.g:985:1: rule__ResourceType__Group__5__Impl : ( ( rule__ResourceType__Group_5__0 )? ) ;
    public final void rule__ResourceType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:989:1: ( ( ( rule__ResourceType__Group_5__0 )? ) )
            // InternalRSec.g:990:1: ( ( rule__ResourceType__Group_5__0 )? )
            {
            // InternalRSec.g:990:1: ( ( rule__ResourceType__Group_5__0 )? )
            // InternalRSec.g:991:2: ( rule__ResourceType__Group_5__0 )?
            {
             before(grammarAccess.getResourceTypeAccess().getGroup_5()); 
            // InternalRSec.g:992:2: ( rule__ResourceType__Group_5__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==22) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalRSec.g:992:3: rule__ResourceType__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ResourceType__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getResourceTypeAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__5__Impl"


    // $ANTLR start "rule__ResourceType__Group__6"
    // InternalRSec.g:1000:1: rule__ResourceType__Group__6 : rule__ResourceType__Group__6__Impl rule__ResourceType__Group__7 ;
    public final void rule__ResourceType__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1004:1: ( rule__ResourceType__Group__6__Impl rule__ResourceType__Group__7 )
            // InternalRSec.g:1005:2: rule__ResourceType__Group__6__Impl rule__ResourceType__Group__7
            {
            pushFollow(FOLLOW_11);
            rule__ResourceType__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__6"


    // $ANTLR start "rule__ResourceType__Group__6__Impl"
    // InternalRSec.g:1012:1: rule__ResourceType__Group__6__Impl : ( ( rule__ResourceType__Group_6__0 )? ) ;
    public final void rule__ResourceType__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1016:1: ( ( ( rule__ResourceType__Group_6__0 )? ) )
            // InternalRSec.g:1017:1: ( ( rule__ResourceType__Group_6__0 )? )
            {
            // InternalRSec.g:1017:1: ( ( rule__ResourceType__Group_6__0 )? )
            // InternalRSec.g:1018:2: ( rule__ResourceType__Group_6__0 )?
            {
             before(grammarAccess.getResourceTypeAccess().getGroup_6()); 
            // InternalRSec.g:1019:2: ( rule__ResourceType__Group_6__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==27) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalRSec.g:1019:3: rule__ResourceType__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ResourceType__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getResourceTypeAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__6__Impl"


    // $ANTLR start "rule__ResourceType__Group__7"
    // InternalRSec.g:1027:1: rule__ResourceType__Group__7 : rule__ResourceType__Group__7__Impl ;
    public final void rule__ResourceType__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1031:1: ( rule__ResourceType__Group__7__Impl )
            // InternalRSec.g:1032:2: rule__ResourceType__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__7"


    // $ANTLR start "rule__ResourceType__Group__7__Impl"
    // InternalRSec.g:1038:1: rule__ResourceType__Group__7__Impl : ( 'end' ) ;
    public final void rule__ResourceType__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1042:1: ( ( 'end' ) )
            // InternalRSec.g:1043:1: ( 'end' )
            {
            // InternalRSec.g:1043:1: ( 'end' )
            // InternalRSec.g:1044:2: 'end'
            {
             before(grammarAccess.getResourceTypeAccess().getEndKeyword_7()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getEndKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group__7__Impl"


    // $ANTLR start "rule__ResourceType__Group_2__0"
    // InternalRSec.g:1054:1: rule__ResourceType__Group_2__0 : rule__ResourceType__Group_2__0__Impl rule__ResourceType__Group_2__1 ;
    public final void rule__ResourceType__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1058:1: ( rule__ResourceType__Group_2__0__Impl rule__ResourceType__Group_2__1 )
            // InternalRSec.g:1059:2: rule__ResourceType__Group_2__0__Impl rule__ResourceType__Group_2__1
            {
            pushFollow(FOLLOW_12);
            rule__ResourceType__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_2__0"


    // $ANTLR start "rule__ResourceType__Group_2__0__Impl"
    // InternalRSec.g:1066:1: rule__ResourceType__Group_2__0__Impl : ( 'on' ) ;
    public final void rule__ResourceType__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1070:1: ( ( 'on' ) )
            // InternalRSec.g:1071:1: ( 'on' )
            {
            // InternalRSec.g:1071:1: ( 'on' )
            // InternalRSec.g:1072:2: 'on'
            {
             before(grammarAccess.getResourceTypeAccess().getOnKeyword_2_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getOnKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_2__0__Impl"


    // $ANTLR start "rule__ResourceType__Group_2__1"
    // InternalRSec.g:1081:1: rule__ResourceType__Group_2__1 : rule__ResourceType__Group_2__1__Impl ;
    public final void rule__ResourceType__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1085:1: ( rule__ResourceType__Group_2__1__Impl )
            // InternalRSec.g:1086:2: rule__ResourceType__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_2__1"


    // $ANTLR start "rule__ResourceType__Group_2__1__Impl"
    // InternalRSec.g:1092:1: rule__ResourceType__Group_2__1__Impl : ( ( rule__ResourceType__PathAssignment_2_1 ) ) ;
    public final void rule__ResourceType__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1096:1: ( ( ( rule__ResourceType__PathAssignment_2_1 ) ) )
            // InternalRSec.g:1097:1: ( ( rule__ResourceType__PathAssignment_2_1 ) )
            {
            // InternalRSec.g:1097:1: ( ( rule__ResourceType__PathAssignment_2_1 ) )
            // InternalRSec.g:1098:2: ( rule__ResourceType__PathAssignment_2_1 )
            {
             before(grammarAccess.getResourceTypeAccess().getPathAssignment_2_1()); 
            // InternalRSec.g:1099:2: ( rule__ResourceType__PathAssignment_2_1 )
            // InternalRSec.g:1099:3: rule__ResourceType__PathAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__PathAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getPathAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_2__1__Impl"


    // $ANTLR start "rule__ResourceType__Group_3__0"
    // InternalRSec.g:1108:1: rule__ResourceType__Group_3__0 : rule__ResourceType__Group_3__0__Impl rule__ResourceType__Group_3__1 ;
    public final void rule__ResourceType__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1112:1: ( rule__ResourceType__Group_3__0__Impl rule__ResourceType__Group_3__1 )
            // InternalRSec.g:1113:2: rule__ResourceType__Group_3__0__Impl rule__ResourceType__Group_3__1
            {
            pushFollow(FOLLOW_12);
            rule__ResourceType__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_3__0"


    // $ANTLR start "rule__ResourceType__Group_3__0__Impl"
    // InternalRSec.g:1120:1: rule__ResourceType__Group_3__0__Impl : ( 'view' ) ;
    public final void rule__ResourceType__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1124:1: ( ( 'view' ) )
            // InternalRSec.g:1125:1: ( 'view' )
            {
            // InternalRSec.g:1125:1: ( 'view' )
            // InternalRSec.g:1126:2: 'view'
            {
             before(grammarAccess.getResourceTypeAccess().getViewKeyword_3_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getViewKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_3__0__Impl"


    // $ANTLR start "rule__ResourceType__Group_3__1"
    // InternalRSec.g:1135:1: rule__ResourceType__Group_3__1 : rule__ResourceType__Group_3__1__Impl ;
    public final void rule__ResourceType__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1139:1: ( rule__ResourceType__Group_3__1__Impl )
            // InternalRSec.g:1140:2: rule__ResourceType__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_3__1"


    // $ANTLR start "rule__ResourceType__Group_3__1__Impl"
    // InternalRSec.g:1146:1: rule__ResourceType__Group_3__1__Impl : ( ( rule__ResourceType__ViewsAssignment_3_1 ) ) ;
    public final void rule__ResourceType__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1150:1: ( ( ( rule__ResourceType__ViewsAssignment_3_1 ) ) )
            // InternalRSec.g:1151:1: ( ( rule__ResourceType__ViewsAssignment_3_1 ) )
            {
            // InternalRSec.g:1151:1: ( ( rule__ResourceType__ViewsAssignment_3_1 ) )
            // InternalRSec.g:1152:2: ( rule__ResourceType__ViewsAssignment_3_1 )
            {
             before(grammarAccess.getResourceTypeAccess().getViewsAssignment_3_1()); 
            // InternalRSec.g:1153:2: ( rule__ResourceType__ViewsAssignment_3_1 )
            // InternalRSec.g:1153:3: rule__ResourceType__ViewsAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__ViewsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getViewsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_3__1__Impl"


    // $ANTLR start "rule__ResourceType__Group_4__0"
    // InternalRSec.g:1162:1: rule__ResourceType__Group_4__0 : rule__ResourceType__Group_4__0__Impl rule__ResourceType__Group_4__1 ;
    public final void rule__ResourceType__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1166:1: ( rule__ResourceType__Group_4__0__Impl rule__ResourceType__Group_4__1 )
            // InternalRSec.g:1167:2: rule__ResourceType__Group_4__0__Impl rule__ResourceType__Group_4__1
            {
            pushFollow(FOLLOW_4);
            rule__ResourceType__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_4__0"


    // $ANTLR start "rule__ResourceType__Group_4__0__Impl"
    // InternalRSec.g:1174:1: rule__ResourceType__Group_4__0__Impl : ( 'data' ) ;
    public final void rule__ResourceType__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1178:1: ( ( 'data' ) )
            // InternalRSec.g:1179:1: ( 'data' )
            {
            // InternalRSec.g:1179:1: ( 'data' )
            // InternalRSec.g:1180:2: 'data'
            {
             before(grammarAccess.getResourceTypeAccess().getDataKeyword_4_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getDataKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_4__0__Impl"


    // $ANTLR start "rule__ResourceType__Group_4__1"
    // InternalRSec.g:1189:1: rule__ResourceType__Group_4__1 : rule__ResourceType__Group_4__1__Impl rule__ResourceType__Group_4__2 ;
    public final void rule__ResourceType__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1193:1: ( rule__ResourceType__Group_4__1__Impl rule__ResourceType__Group_4__2 )
            // InternalRSec.g:1194:2: rule__ResourceType__Group_4__1__Impl rule__ResourceType__Group_4__2
            {
            pushFollow(FOLLOW_13);
            rule__ResourceType__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_4__1"


    // $ANTLR start "rule__ResourceType__Group_4__1__Impl"
    // InternalRSec.g:1201:1: rule__ResourceType__Group_4__1__Impl : ( ( ( rule__ResourceType__PropertiesAssignment_4_1 ) ) ( ( rule__ResourceType__PropertiesAssignment_4_1 )* ) ) ;
    public final void rule__ResourceType__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1205:1: ( ( ( ( rule__ResourceType__PropertiesAssignment_4_1 ) ) ( ( rule__ResourceType__PropertiesAssignment_4_1 )* ) ) )
            // InternalRSec.g:1206:1: ( ( ( rule__ResourceType__PropertiesAssignment_4_1 ) ) ( ( rule__ResourceType__PropertiesAssignment_4_1 )* ) )
            {
            // InternalRSec.g:1206:1: ( ( ( rule__ResourceType__PropertiesAssignment_4_1 ) ) ( ( rule__ResourceType__PropertiesAssignment_4_1 )* ) )
            // InternalRSec.g:1207:2: ( ( rule__ResourceType__PropertiesAssignment_4_1 ) ) ( ( rule__ResourceType__PropertiesAssignment_4_1 )* )
            {
            // InternalRSec.g:1207:2: ( ( rule__ResourceType__PropertiesAssignment_4_1 ) )
            // InternalRSec.g:1208:3: ( rule__ResourceType__PropertiesAssignment_4_1 )
            {
             before(grammarAccess.getResourceTypeAccess().getPropertiesAssignment_4_1()); 
            // InternalRSec.g:1209:3: ( rule__ResourceType__PropertiesAssignment_4_1 )
            // InternalRSec.g:1209:4: rule__ResourceType__PropertiesAssignment_4_1
            {
            pushFollow(FOLLOW_14);
            rule__ResourceType__PropertiesAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getPropertiesAssignment_4_1()); 

            }

            // InternalRSec.g:1212:2: ( ( rule__ResourceType__PropertiesAssignment_4_1 )* )
            // InternalRSec.g:1213:3: ( rule__ResourceType__PropertiesAssignment_4_1 )*
            {
             before(grammarAccess.getResourceTypeAccess().getPropertiesAssignment_4_1()); 
            // InternalRSec.g:1214:3: ( rule__ResourceType__PropertiesAssignment_4_1 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalRSec.g:1214:4: rule__ResourceType__PropertiesAssignment_4_1
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__ResourceType__PropertiesAssignment_4_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getResourceTypeAccess().getPropertiesAssignment_4_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_4__1__Impl"


    // $ANTLR start "rule__ResourceType__Group_4__2"
    // InternalRSec.g:1223:1: rule__ResourceType__Group_4__2 : rule__ResourceType__Group_4__2__Impl ;
    public final void rule__ResourceType__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1227:1: ( rule__ResourceType__Group_4__2__Impl )
            // InternalRSec.g:1228:2: rule__ResourceType__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_4__2"


    // $ANTLR start "rule__ResourceType__Group_4__2__Impl"
    // InternalRSec.g:1234:1: rule__ResourceType__Group_4__2__Impl : ( 'end' ) ;
    public final void rule__ResourceType__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1238:1: ( ( 'end' ) )
            // InternalRSec.g:1239:1: ( 'end' )
            {
            // InternalRSec.g:1239:1: ( 'end' )
            // InternalRSec.g:1240:2: 'end'
            {
             before(grammarAccess.getResourceTypeAccess().getEndKeyword_4_2()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getEndKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_4__2__Impl"


    // $ANTLR start "rule__ResourceType__Group_5__0"
    // InternalRSec.g:1250:1: rule__ResourceType__Group_5__0 : rule__ResourceType__Group_5__0__Impl rule__ResourceType__Group_5__1 ;
    public final void rule__ResourceType__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1254:1: ( rule__ResourceType__Group_5__0__Impl rule__ResourceType__Group_5__1 )
            // InternalRSec.g:1255:2: rule__ResourceType__Group_5__0__Impl rule__ResourceType__Group_5__1
            {
            pushFollow(FOLLOW_15);
            rule__ResourceType__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5__0"


    // $ANTLR start "rule__ResourceType__Group_5__0__Impl"
    // InternalRSec.g:1262:1: rule__ResourceType__Group_5__0__Impl : ( 'actions' ) ;
    public final void rule__ResourceType__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1266:1: ( ( 'actions' ) )
            // InternalRSec.g:1267:1: ( 'actions' )
            {
            // InternalRSec.g:1267:1: ( 'actions' )
            // InternalRSec.g:1268:2: 'actions'
            {
             before(grammarAccess.getResourceTypeAccess().getActionsKeyword_5_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getActionsKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5__0__Impl"


    // $ANTLR start "rule__ResourceType__Group_5__1"
    // InternalRSec.g:1277:1: rule__ResourceType__Group_5__1 : rule__ResourceType__Group_5__1__Impl rule__ResourceType__Group_5__2 ;
    public final void rule__ResourceType__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1281:1: ( rule__ResourceType__Group_5__1__Impl rule__ResourceType__Group_5__2 )
            // InternalRSec.g:1282:2: rule__ResourceType__Group_5__1__Impl rule__ResourceType__Group_5__2
            {
            pushFollow(FOLLOW_15);
            rule__ResourceType__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5__1"


    // $ANTLR start "rule__ResourceType__Group_5__1__Impl"
    // InternalRSec.g:1289:1: rule__ResourceType__Group_5__1__Impl : ( ( rule__ResourceType__Group_5_1__0 )? ) ;
    public final void rule__ResourceType__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1293:1: ( ( ( rule__ResourceType__Group_5_1__0 )? ) )
            // InternalRSec.g:1294:1: ( ( rule__ResourceType__Group_5_1__0 )? )
            {
            // InternalRSec.g:1294:1: ( ( rule__ResourceType__Group_5_1__0 )? )
            // InternalRSec.g:1295:2: ( rule__ResourceType__Group_5_1__0 )?
            {
             before(grammarAccess.getResourceTypeAccess().getGroup_5_1()); 
            // InternalRSec.g:1296:2: ( rule__ResourceType__Group_5_1__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==23) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalRSec.g:1296:3: rule__ResourceType__Group_5_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ResourceType__Group_5_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getResourceTypeAccess().getGroup_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5__1__Impl"


    // $ANTLR start "rule__ResourceType__Group_5__2"
    // InternalRSec.g:1304:1: rule__ResourceType__Group_5__2 : rule__ResourceType__Group_5__2__Impl rule__ResourceType__Group_5__3 ;
    public final void rule__ResourceType__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1308:1: ( rule__ResourceType__Group_5__2__Impl rule__ResourceType__Group_5__3 )
            // InternalRSec.g:1309:2: rule__ResourceType__Group_5__2__Impl rule__ResourceType__Group_5__3
            {
            pushFollow(FOLLOW_13);
            rule__ResourceType__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5__2"


    // $ANTLR start "rule__ResourceType__Group_5__2__Impl"
    // InternalRSec.g:1316:1: rule__ResourceType__Group_5__2__Impl : ( ( ( rule__ResourceType__EventsAssignment_5_2 ) ) ( ( rule__ResourceType__EventsAssignment_5_2 )* ) ) ;
    public final void rule__ResourceType__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1320:1: ( ( ( ( rule__ResourceType__EventsAssignment_5_2 ) ) ( ( rule__ResourceType__EventsAssignment_5_2 )* ) ) )
            // InternalRSec.g:1321:1: ( ( ( rule__ResourceType__EventsAssignment_5_2 ) ) ( ( rule__ResourceType__EventsAssignment_5_2 )* ) )
            {
            // InternalRSec.g:1321:1: ( ( ( rule__ResourceType__EventsAssignment_5_2 ) ) ( ( rule__ResourceType__EventsAssignment_5_2 )* ) )
            // InternalRSec.g:1322:2: ( ( rule__ResourceType__EventsAssignment_5_2 ) ) ( ( rule__ResourceType__EventsAssignment_5_2 )* )
            {
            // InternalRSec.g:1322:2: ( ( rule__ResourceType__EventsAssignment_5_2 ) )
            // InternalRSec.g:1323:3: ( rule__ResourceType__EventsAssignment_5_2 )
            {
             before(grammarAccess.getResourceTypeAccess().getEventsAssignment_5_2()); 
            // InternalRSec.g:1324:3: ( rule__ResourceType__EventsAssignment_5_2 )
            // InternalRSec.g:1324:4: rule__ResourceType__EventsAssignment_5_2
            {
            pushFollow(FOLLOW_16);
            rule__ResourceType__EventsAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getEventsAssignment_5_2()); 

            }

            // InternalRSec.g:1327:2: ( ( rule__ResourceType__EventsAssignment_5_2 )* )
            // InternalRSec.g:1328:3: ( rule__ResourceType__EventsAssignment_5_2 )*
            {
             before(grammarAccess.getResourceTypeAccess().getEventsAssignment_5_2()); 
            // InternalRSec.g:1329:3: ( rule__ResourceType__EventsAssignment_5_2 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_ID) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalRSec.g:1329:4: rule__ResourceType__EventsAssignment_5_2
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__ResourceType__EventsAssignment_5_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getResourceTypeAccess().getEventsAssignment_5_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5__2__Impl"


    // $ANTLR start "rule__ResourceType__Group_5__3"
    // InternalRSec.g:1338:1: rule__ResourceType__Group_5__3 : rule__ResourceType__Group_5__3__Impl ;
    public final void rule__ResourceType__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1342:1: ( rule__ResourceType__Group_5__3__Impl )
            // InternalRSec.g:1343:2: rule__ResourceType__Group_5__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5__3"


    // $ANTLR start "rule__ResourceType__Group_5__3__Impl"
    // InternalRSec.g:1349:1: rule__ResourceType__Group_5__3__Impl : ( 'end' ) ;
    public final void rule__ResourceType__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1353:1: ( ( 'end' ) )
            // InternalRSec.g:1354:1: ( 'end' )
            {
            // InternalRSec.g:1354:1: ( 'end' )
            // InternalRSec.g:1355:2: 'end'
            {
             before(grammarAccess.getResourceTypeAccess().getEndKeyword_5_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getEndKeyword_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5__3__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1__0"
    // InternalRSec.g:1365:1: rule__ResourceType__Group_5_1__0 : rule__ResourceType__Group_5_1__0__Impl rule__ResourceType__Group_5_1__1 ;
    public final void rule__ResourceType__Group_5_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1369:1: ( rule__ResourceType__Group_5_1__0__Impl rule__ResourceType__Group_5_1__1 )
            // InternalRSec.g:1370:2: rule__ResourceType__Group_5_1__0__Impl rule__ResourceType__Group_5_1__1
            {
            pushFollow(FOLLOW_12);
            rule__ResourceType__Group_5_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__0"


    // $ANTLR start "rule__ResourceType__Group_5_1__0__Impl"
    // InternalRSec.g:1377:1: rule__ResourceType__Group_5_1__0__Impl : ( '{' ) ;
    public final void rule__ResourceType__Group_5_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1381:1: ( ( '{' ) )
            // InternalRSec.g:1382:1: ( '{' )
            {
            // InternalRSec.g:1382:1: ( '{' )
            // InternalRSec.g:1383:2: '{'
            {
             before(grammarAccess.getResourceTypeAccess().getLeftCurlyBracketKeyword_5_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getLeftCurlyBracketKeyword_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__0__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1__1"
    // InternalRSec.g:1392:1: rule__ResourceType__Group_5_1__1 : rule__ResourceType__Group_5_1__1__Impl rule__ResourceType__Group_5_1__2 ;
    public final void rule__ResourceType__Group_5_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1396:1: ( rule__ResourceType__Group_5_1__1__Impl rule__ResourceType__Group_5_1__2 )
            // InternalRSec.g:1397:2: rule__ResourceType__Group_5_1__1__Impl rule__ResourceType__Group_5_1__2
            {
            pushFollow(FOLLOW_17);
            rule__ResourceType__Group_5_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__1"


    // $ANTLR start "rule__ResourceType__Group_5_1__1__Impl"
    // InternalRSec.g:1404:1: rule__ResourceType__Group_5_1__1__Impl : ( ( rule__ResourceType__PathvariableAssignment_5_1_1 ) ) ;
    public final void rule__ResourceType__Group_5_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1408:1: ( ( ( rule__ResourceType__PathvariableAssignment_5_1_1 ) ) )
            // InternalRSec.g:1409:1: ( ( rule__ResourceType__PathvariableAssignment_5_1_1 ) )
            {
            // InternalRSec.g:1409:1: ( ( rule__ResourceType__PathvariableAssignment_5_1_1 ) )
            // InternalRSec.g:1410:2: ( rule__ResourceType__PathvariableAssignment_5_1_1 )
            {
             before(grammarAccess.getResourceTypeAccess().getPathvariableAssignment_5_1_1()); 
            // InternalRSec.g:1411:2: ( rule__ResourceType__PathvariableAssignment_5_1_1 )
            // InternalRSec.g:1411:3: rule__ResourceType__PathvariableAssignment_5_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__PathvariableAssignment_5_1_1();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getPathvariableAssignment_5_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__1__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1__2"
    // InternalRSec.g:1419:1: rule__ResourceType__Group_5_1__2 : rule__ResourceType__Group_5_1__2__Impl rule__ResourceType__Group_5_1__3 ;
    public final void rule__ResourceType__Group_5_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1423:1: ( rule__ResourceType__Group_5_1__2__Impl rule__ResourceType__Group_5_1__3 )
            // InternalRSec.g:1424:2: rule__ResourceType__Group_5_1__2__Impl rule__ResourceType__Group_5_1__3
            {
            pushFollow(FOLLOW_4);
            rule__ResourceType__Group_5_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__2"


    // $ANTLR start "rule__ResourceType__Group_5_1__2__Impl"
    // InternalRSec.g:1431:1: rule__ResourceType__Group_5_1__2__Impl : ( '->' ) ;
    public final void rule__ResourceType__Group_5_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1435:1: ( ( '->' ) )
            // InternalRSec.g:1436:1: ( '->' )
            {
            // InternalRSec.g:1436:1: ( '->' )
            // InternalRSec.g:1437:2: '->'
            {
             before(grammarAccess.getResourceTypeAccess().getHyphenMinusGreaterThanSignKeyword_5_1_2()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getHyphenMinusGreaterThanSignKeyword_5_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__2__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1__3"
    // InternalRSec.g:1446:1: rule__ResourceType__Group_5_1__3 : rule__ResourceType__Group_5_1__3__Impl rule__ResourceType__Group_5_1__4 ;
    public final void rule__ResourceType__Group_5_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1450:1: ( rule__ResourceType__Group_5_1__3__Impl rule__ResourceType__Group_5_1__4 )
            // InternalRSec.g:1451:2: rule__ResourceType__Group_5_1__3__Impl rule__ResourceType__Group_5_1__4
            {
            pushFollow(FOLLOW_18);
            rule__ResourceType__Group_5_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__3"


    // $ANTLR start "rule__ResourceType__Group_5_1__3__Impl"
    // InternalRSec.g:1458:1: rule__ResourceType__Group_5_1__3__Impl : ( ( rule__ResourceType__ResourceAssignment_5_1_3 ) ) ;
    public final void rule__ResourceType__Group_5_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1462:1: ( ( ( rule__ResourceType__ResourceAssignment_5_1_3 ) ) )
            // InternalRSec.g:1463:1: ( ( rule__ResourceType__ResourceAssignment_5_1_3 ) )
            {
            // InternalRSec.g:1463:1: ( ( rule__ResourceType__ResourceAssignment_5_1_3 ) )
            // InternalRSec.g:1464:2: ( rule__ResourceType__ResourceAssignment_5_1_3 )
            {
             before(grammarAccess.getResourceTypeAccess().getResourceAssignment_5_1_3()); 
            // InternalRSec.g:1465:2: ( rule__ResourceType__ResourceAssignment_5_1_3 )
            // InternalRSec.g:1465:3: rule__ResourceType__ResourceAssignment_5_1_3
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__ResourceAssignment_5_1_3();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getResourceAssignment_5_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__3__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1__4"
    // InternalRSec.g:1473:1: rule__ResourceType__Group_5_1__4 : rule__ResourceType__Group_5_1__4__Impl rule__ResourceType__Group_5_1__5 ;
    public final void rule__ResourceType__Group_5_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1477:1: ( rule__ResourceType__Group_5_1__4__Impl rule__ResourceType__Group_5_1__5 )
            // InternalRSec.g:1478:2: rule__ResourceType__Group_5_1__4__Impl rule__ResourceType__Group_5_1__5
            {
            pushFollow(FOLLOW_18);
            rule__ResourceType__Group_5_1__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__4"


    // $ANTLR start "rule__ResourceType__Group_5_1__4__Impl"
    // InternalRSec.g:1485:1: rule__ResourceType__Group_5_1__4__Impl : ( ( rule__ResourceType__Group_5_1_4__0 )* ) ;
    public final void rule__ResourceType__Group_5_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1489:1: ( ( ( rule__ResourceType__Group_5_1_4__0 )* ) )
            // InternalRSec.g:1490:1: ( ( rule__ResourceType__Group_5_1_4__0 )* )
            {
            // InternalRSec.g:1490:1: ( ( rule__ResourceType__Group_5_1_4__0 )* )
            // InternalRSec.g:1491:2: ( rule__ResourceType__Group_5_1_4__0 )*
            {
             before(grammarAccess.getResourceTypeAccess().getGroup_5_1_4()); 
            // InternalRSec.g:1492:2: ( rule__ResourceType__Group_5_1_4__0 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==26) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalRSec.g:1492:3: rule__ResourceType__Group_5_1_4__0
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__ResourceType__Group_5_1_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getResourceTypeAccess().getGroup_5_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__4__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1__5"
    // InternalRSec.g:1500:1: rule__ResourceType__Group_5_1__5 : rule__ResourceType__Group_5_1__5__Impl ;
    public final void rule__ResourceType__Group_5_1__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1504:1: ( rule__ResourceType__Group_5_1__5__Impl )
            // InternalRSec.g:1505:2: rule__ResourceType__Group_5_1__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__5"


    // $ANTLR start "rule__ResourceType__Group_5_1__5__Impl"
    // InternalRSec.g:1511:1: rule__ResourceType__Group_5_1__5__Impl : ( '}' ) ;
    public final void rule__ResourceType__Group_5_1__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1515:1: ( ( '}' ) )
            // InternalRSec.g:1516:1: ( '}' )
            {
            // InternalRSec.g:1516:1: ( '}' )
            // InternalRSec.g:1517:2: '}'
            {
             before(grammarAccess.getResourceTypeAccess().getRightCurlyBracketKeyword_5_1_5()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getRightCurlyBracketKeyword_5_1_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1__5__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1_4__0"
    // InternalRSec.g:1527:1: rule__ResourceType__Group_5_1_4__0 : rule__ResourceType__Group_5_1_4__0__Impl rule__ResourceType__Group_5_1_4__1 ;
    public final void rule__ResourceType__Group_5_1_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1531:1: ( rule__ResourceType__Group_5_1_4__0__Impl rule__ResourceType__Group_5_1_4__1 )
            // InternalRSec.g:1532:2: rule__ResourceType__Group_5_1_4__0__Impl rule__ResourceType__Group_5_1_4__1
            {
            pushFollow(FOLLOW_12);
            rule__ResourceType__Group_5_1_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1_4__0"


    // $ANTLR start "rule__ResourceType__Group_5_1_4__0__Impl"
    // InternalRSec.g:1539:1: rule__ResourceType__Group_5_1_4__0__Impl : ( ',' ) ;
    public final void rule__ResourceType__Group_5_1_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1543:1: ( ( ',' ) )
            // InternalRSec.g:1544:1: ( ',' )
            {
            // InternalRSec.g:1544:1: ( ',' )
            // InternalRSec.g:1545:2: ','
            {
             before(grammarAccess.getResourceTypeAccess().getCommaKeyword_5_1_4_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getCommaKeyword_5_1_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1_4__0__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1_4__1"
    // InternalRSec.g:1554:1: rule__ResourceType__Group_5_1_4__1 : rule__ResourceType__Group_5_1_4__1__Impl rule__ResourceType__Group_5_1_4__2 ;
    public final void rule__ResourceType__Group_5_1_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1558:1: ( rule__ResourceType__Group_5_1_4__1__Impl rule__ResourceType__Group_5_1_4__2 )
            // InternalRSec.g:1559:2: rule__ResourceType__Group_5_1_4__1__Impl rule__ResourceType__Group_5_1_4__2
            {
            pushFollow(FOLLOW_17);
            rule__ResourceType__Group_5_1_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1_4__1"


    // $ANTLR start "rule__ResourceType__Group_5_1_4__1__Impl"
    // InternalRSec.g:1566:1: rule__ResourceType__Group_5_1_4__1__Impl : ( ( rule__ResourceType__PathvariableAssignment_5_1_4_1 ) ) ;
    public final void rule__ResourceType__Group_5_1_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1570:1: ( ( ( rule__ResourceType__PathvariableAssignment_5_1_4_1 ) ) )
            // InternalRSec.g:1571:1: ( ( rule__ResourceType__PathvariableAssignment_5_1_4_1 ) )
            {
            // InternalRSec.g:1571:1: ( ( rule__ResourceType__PathvariableAssignment_5_1_4_1 ) )
            // InternalRSec.g:1572:2: ( rule__ResourceType__PathvariableAssignment_5_1_4_1 )
            {
             before(grammarAccess.getResourceTypeAccess().getPathvariableAssignment_5_1_4_1()); 
            // InternalRSec.g:1573:2: ( rule__ResourceType__PathvariableAssignment_5_1_4_1 )
            // InternalRSec.g:1573:3: rule__ResourceType__PathvariableAssignment_5_1_4_1
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__PathvariableAssignment_5_1_4_1();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getPathvariableAssignment_5_1_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1_4__1__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1_4__2"
    // InternalRSec.g:1581:1: rule__ResourceType__Group_5_1_4__2 : rule__ResourceType__Group_5_1_4__2__Impl rule__ResourceType__Group_5_1_4__3 ;
    public final void rule__ResourceType__Group_5_1_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1585:1: ( rule__ResourceType__Group_5_1_4__2__Impl rule__ResourceType__Group_5_1_4__3 )
            // InternalRSec.g:1586:2: rule__ResourceType__Group_5_1_4__2__Impl rule__ResourceType__Group_5_1_4__3
            {
            pushFollow(FOLLOW_4);
            rule__ResourceType__Group_5_1_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1_4__2"


    // $ANTLR start "rule__ResourceType__Group_5_1_4__2__Impl"
    // InternalRSec.g:1593:1: rule__ResourceType__Group_5_1_4__2__Impl : ( '->' ) ;
    public final void rule__ResourceType__Group_5_1_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1597:1: ( ( '->' ) )
            // InternalRSec.g:1598:1: ( '->' )
            {
            // InternalRSec.g:1598:1: ( '->' )
            // InternalRSec.g:1599:2: '->'
            {
             before(grammarAccess.getResourceTypeAccess().getHyphenMinusGreaterThanSignKeyword_5_1_4_2()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getHyphenMinusGreaterThanSignKeyword_5_1_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1_4__2__Impl"


    // $ANTLR start "rule__ResourceType__Group_5_1_4__3"
    // InternalRSec.g:1608:1: rule__ResourceType__Group_5_1_4__3 : rule__ResourceType__Group_5_1_4__3__Impl ;
    public final void rule__ResourceType__Group_5_1_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1612:1: ( rule__ResourceType__Group_5_1_4__3__Impl )
            // InternalRSec.g:1613:2: rule__ResourceType__Group_5_1_4__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_5_1_4__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1_4__3"


    // $ANTLR start "rule__ResourceType__Group_5_1_4__3__Impl"
    // InternalRSec.g:1619:1: rule__ResourceType__Group_5_1_4__3__Impl : ( ( rule__ResourceType__ResourceAssignment_5_1_4_3 ) ) ;
    public final void rule__ResourceType__Group_5_1_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1623:1: ( ( ( rule__ResourceType__ResourceAssignment_5_1_4_3 ) ) )
            // InternalRSec.g:1624:1: ( ( rule__ResourceType__ResourceAssignment_5_1_4_3 ) )
            {
            // InternalRSec.g:1624:1: ( ( rule__ResourceType__ResourceAssignment_5_1_4_3 ) )
            // InternalRSec.g:1625:2: ( rule__ResourceType__ResourceAssignment_5_1_4_3 )
            {
             before(grammarAccess.getResourceTypeAccess().getResourceAssignment_5_1_4_3()); 
            // InternalRSec.g:1626:2: ( rule__ResourceType__ResourceAssignment_5_1_4_3 )
            // InternalRSec.g:1626:3: rule__ResourceType__ResourceAssignment_5_1_4_3
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__ResourceAssignment_5_1_4_3();

            state._fsp--;


            }

             after(grammarAccess.getResourceTypeAccess().getResourceAssignment_5_1_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_5_1_4__3__Impl"


    // $ANTLR start "rule__ResourceType__Group_6__0"
    // InternalRSec.g:1635:1: rule__ResourceType__Group_6__0 : rule__ResourceType__Group_6__0__Impl rule__ResourceType__Group_6__1 ;
    public final void rule__ResourceType__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1639:1: ( rule__ResourceType__Group_6__0__Impl rule__ResourceType__Group_6__1 )
            // InternalRSec.g:1640:2: rule__ResourceType__Group_6__0__Impl rule__ResourceType__Group_6__1
            {
            pushFollow(FOLLOW_20);
            rule__ResourceType__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_6__0"


    // $ANTLR start "rule__ResourceType__Group_6__0__Impl"
    // InternalRSec.g:1647:1: rule__ResourceType__Group_6__0__Impl : ( 'states' ) ;
    public final void rule__ResourceType__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1651:1: ( ( 'states' ) )
            // InternalRSec.g:1652:1: ( 'states' )
            {
            // InternalRSec.g:1652:1: ( 'states' )
            // InternalRSec.g:1653:2: 'states'
            {
             before(grammarAccess.getResourceTypeAccess().getStatesKeyword_6_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getStatesKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_6__0__Impl"


    // $ANTLR start "rule__ResourceType__Group_6__1"
    // InternalRSec.g:1662:1: rule__ResourceType__Group_6__1 : rule__ResourceType__Group_6__1__Impl rule__ResourceType__Group_6__2 ;
    public final void rule__ResourceType__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1666:1: ( rule__ResourceType__Group_6__1__Impl rule__ResourceType__Group_6__2 )
            // InternalRSec.g:1667:2: rule__ResourceType__Group_6__1__Impl rule__ResourceType__Group_6__2
            {
            pushFollow(FOLLOW_20);
            rule__ResourceType__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_6__1"


    // $ANTLR start "rule__ResourceType__Group_6__1__Impl"
    // InternalRSec.g:1674:1: rule__ResourceType__Group_6__1__Impl : ( ( rule__ResourceType__StatesAssignment_6_1 )* ) ;
    public final void rule__ResourceType__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1678:1: ( ( ( rule__ResourceType__StatesAssignment_6_1 )* ) )
            // InternalRSec.g:1679:1: ( ( rule__ResourceType__StatesAssignment_6_1 )* )
            {
            // InternalRSec.g:1679:1: ( ( rule__ResourceType__StatesAssignment_6_1 )* )
            // InternalRSec.g:1680:2: ( rule__ResourceType__StatesAssignment_6_1 )*
            {
             before(grammarAccess.getResourceTypeAccess().getStatesAssignment_6_1()); 
            // InternalRSec.g:1681:2: ( rule__ResourceType__StatesAssignment_6_1 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==34) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalRSec.g:1681:3: rule__ResourceType__StatesAssignment_6_1
            	    {
            	    pushFollow(FOLLOW_21);
            	    rule__ResourceType__StatesAssignment_6_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getResourceTypeAccess().getStatesAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_6__1__Impl"


    // $ANTLR start "rule__ResourceType__Group_6__2"
    // InternalRSec.g:1689:1: rule__ResourceType__Group_6__2 : rule__ResourceType__Group_6__2__Impl ;
    public final void rule__ResourceType__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1693:1: ( rule__ResourceType__Group_6__2__Impl )
            // InternalRSec.g:1694:2: rule__ResourceType__Group_6__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResourceType__Group_6__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_6__2"


    // $ANTLR start "rule__ResourceType__Group_6__2__Impl"
    // InternalRSec.g:1700:1: rule__ResourceType__Group_6__2__Impl : ( 'end' ) ;
    public final void rule__ResourceType__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1704:1: ( ( 'end' ) )
            // InternalRSec.g:1705:1: ( 'end' )
            {
            // InternalRSec.g:1705:1: ( 'end' )
            // InternalRSec.g:1706:2: 'end'
            {
             before(grammarAccess.getResourceTypeAccess().getEndKeyword_6_2()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getEndKeyword_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__Group_6__2__Impl"


    // $ANTLR start "rule__Event__Group__0"
    // InternalRSec.g:1716:1: rule__Event__Group__0 : rule__Event__Group__0__Impl rule__Event__Group__1 ;
    public final void rule__Event__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1720:1: ( rule__Event__Group__0__Impl rule__Event__Group__1 )
            // InternalRSec.g:1721:2: rule__Event__Group__0__Impl rule__Event__Group__1
            {
            pushFollow(FOLLOW_22);
            rule__Event__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__0"


    // $ANTLR start "rule__Event__Group__0__Impl"
    // InternalRSec.g:1728:1: rule__Event__Group__0__Impl : ( ( rule__Event__NameAssignment_0 ) ) ;
    public final void rule__Event__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1732:1: ( ( ( rule__Event__NameAssignment_0 ) ) )
            // InternalRSec.g:1733:1: ( ( rule__Event__NameAssignment_0 ) )
            {
            // InternalRSec.g:1733:1: ( ( rule__Event__NameAssignment_0 ) )
            // InternalRSec.g:1734:2: ( rule__Event__NameAssignment_0 )
            {
             before(grammarAccess.getEventAccess().getNameAssignment_0()); 
            // InternalRSec.g:1735:2: ( rule__Event__NameAssignment_0 )
            // InternalRSec.g:1735:3: rule__Event__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Event__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__0__Impl"


    // $ANTLR start "rule__Event__Group__1"
    // InternalRSec.g:1743:1: rule__Event__Group__1 : rule__Event__Group__1__Impl rule__Event__Group__2 ;
    public final void rule__Event__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1747:1: ( rule__Event__Group__1__Impl rule__Event__Group__2 )
            // InternalRSec.g:1748:2: rule__Event__Group__1__Impl rule__Event__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__Event__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__1"


    // $ANTLR start "rule__Event__Group__1__Impl"
    // InternalRSec.g:1755:1: rule__Event__Group__1__Impl : ( '(' ) ;
    public final void rule__Event__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1759:1: ( ( '(' ) )
            // InternalRSec.g:1760:1: ( '(' )
            {
            // InternalRSec.g:1760:1: ( '(' )
            // InternalRSec.g:1761:2: '('
            {
             before(grammarAccess.getEventAccess().getLeftParenthesisKeyword_1()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__1__Impl"


    // $ANTLR start "rule__Event__Group__2"
    // InternalRSec.g:1770:1: rule__Event__Group__2 : rule__Event__Group__2__Impl rule__Event__Group__3 ;
    public final void rule__Event__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1774:1: ( rule__Event__Group__2__Impl rule__Event__Group__3 )
            // InternalRSec.g:1775:2: rule__Event__Group__2__Impl rule__Event__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__Event__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__2"


    // $ANTLR start "rule__Event__Group__2__Impl"
    // InternalRSec.g:1782:1: rule__Event__Group__2__Impl : ( ( rule__Event__ParamTypeAssignment_2 )? ) ;
    public final void rule__Event__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1786:1: ( ( ( rule__Event__ParamTypeAssignment_2 )? ) )
            // InternalRSec.g:1787:1: ( ( rule__Event__ParamTypeAssignment_2 )? )
            {
            // InternalRSec.g:1787:1: ( ( rule__Event__ParamTypeAssignment_2 )? )
            // InternalRSec.g:1788:2: ( rule__Event__ParamTypeAssignment_2 )?
            {
             before(grammarAccess.getEventAccess().getParamTypeAssignment_2()); 
            // InternalRSec.g:1789:2: ( rule__Event__ParamTypeAssignment_2 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalRSec.g:1789:3: rule__Event__ParamTypeAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__ParamTypeAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getParamTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__2__Impl"


    // $ANTLR start "rule__Event__Group__3"
    // InternalRSec.g:1797:1: rule__Event__Group__3 : rule__Event__Group__3__Impl rule__Event__Group__4 ;
    public final void rule__Event__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1801:1: ( rule__Event__Group__3__Impl rule__Event__Group__4 )
            // InternalRSec.g:1802:2: rule__Event__Group__3__Impl rule__Event__Group__4
            {
            pushFollow(FOLLOW_24);
            rule__Event__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__3"


    // $ANTLR start "rule__Event__Group__3__Impl"
    // InternalRSec.g:1809:1: rule__Event__Group__3__Impl : ( ')' ) ;
    public final void rule__Event__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1813:1: ( ( ')' ) )
            // InternalRSec.g:1814:1: ( ')' )
            {
            // InternalRSec.g:1814:1: ( ')' )
            // InternalRSec.g:1815:2: ')'
            {
             before(grammarAccess.getEventAccess().getRightParenthesisKeyword_3()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__3__Impl"


    // $ANTLR start "rule__Event__Group__4"
    // InternalRSec.g:1824:1: rule__Event__Group__4 : rule__Event__Group__4__Impl rule__Event__Group__5 ;
    public final void rule__Event__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1828:1: ( rule__Event__Group__4__Impl rule__Event__Group__5 )
            // InternalRSec.g:1829:2: rule__Event__Group__4__Impl rule__Event__Group__5
            {
            pushFollow(FOLLOW_25);
            rule__Event__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__4"


    // $ANTLR start "rule__Event__Group__4__Impl"
    // InternalRSec.g:1836:1: rule__Event__Group__4__Impl : ( ':' ) ;
    public final void rule__Event__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1840:1: ( ( ':' ) )
            // InternalRSec.g:1841:1: ( ':' )
            {
            // InternalRSec.g:1841:1: ( ':' )
            // InternalRSec.g:1842:2: ':'
            {
             before(grammarAccess.getEventAccess().getColonKeyword_4()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getColonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__4__Impl"


    // $ANTLR start "rule__Event__Group__5"
    // InternalRSec.g:1851:1: rule__Event__Group__5 : rule__Event__Group__5__Impl rule__Event__Group__6 ;
    public final void rule__Event__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1855:1: ( rule__Event__Group__5__Impl rule__Event__Group__6 )
            // InternalRSec.g:1856:2: rule__Event__Group__5__Impl rule__Event__Group__6
            {
            pushFollow(FOLLOW_25);
            rule__Event__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__5"


    // $ANTLR start "rule__Event__Group__5__Impl"
    // InternalRSec.g:1863:1: rule__Event__Group__5__Impl : ( ( rule__Event__ManyAssignment_5 )? ) ;
    public final void rule__Event__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1867:1: ( ( ( rule__Event__ManyAssignment_5 )? ) )
            // InternalRSec.g:1868:1: ( ( rule__Event__ManyAssignment_5 )? )
            {
            // InternalRSec.g:1868:1: ( ( rule__Event__ManyAssignment_5 )? )
            // InternalRSec.g:1869:2: ( rule__Event__ManyAssignment_5 )?
            {
             before(grammarAccess.getEventAccess().getManyAssignment_5()); 
            // InternalRSec.g:1870:2: ( rule__Event__ManyAssignment_5 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==36) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalRSec.g:1870:3: rule__Event__ManyAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__ManyAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getManyAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__5__Impl"


    // $ANTLR start "rule__Event__Group__6"
    // InternalRSec.g:1878:1: rule__Event__Group__6 : rule__Event__Group__6__Impl rule__Event__Group__7 ;
    public final void rule__Event__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1882:1: ( rule__Event__Group__6__Impl rule__Event__Group__7 )
            // InternalRSec.g:1883:2: rule__Event__Group__6__Impl rule__Event__Group__7
            {
            pushFollow(FOLLOW_26);
            rule__Event__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__6"


    // $ANTLR start "rule__Event__Group__6__Impl"
    // InternalRSec.g:1890:1: rule__Event__Group__6__Impl : ( ( rule__Event__ReturnTypeAssignment_6 ) ) ;
    public final void rule__Event__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1894:1: ( ( ( rule__Event__ReturnTypeAssignment_6 ) ) )
            // InternalRSec.g:1895:1: ( ( rule__Event__ReturnTypeAssignment_6 ) )
            {
            // InternalRSec.g:1895:1: ( ( rule__Event__ReturnTypeAssignment_6 ) )
            // InternalRSec.g:1896:2: ( rule__Event__ReturnTypeAssignment_6 )
            {
             before(grammarAccess.getEventAccess().getReturnTypeAssignment_6()); 
            // InternalRSec.g:1897:2: ( rule__Event__ReturnTypeAssignment_6 )
            // InternalRSec.g:1897:3: rule__Event__ReturnTypeAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__Event__ReturnTypeAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getReturnTypeAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__6__Impl"


    // $ANTLR start "rule__Event__Group__7"
    // InternalRSec.g:1905:1: rule__Event__Group__7 : rule__Event__Group__7__Impl rule__Event__Group__8 ;
    public final void rule__Event__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1909:1: ( rule__Event__Group__7__Impl rule__Event__Group__8 )
            // InternalRSec.g:1910:2: rule__Event__Group__7__Impl rule__Event__Group__8
            {
            pushFollow(FOLLOW_4);
            rule__Event__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__7"


    // $ANTLR start "rule__Event__Group__7__Impl"
    // InternalRSec.g:1917:1: rule__Event__Group__7__Impl : ( 'with' ) ;
    public final void rule__Event__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1921:1: ( ( 'with' ) )
            // InternalRSec.g:1922:1: ( 'with' )
            {
            // InternalRSec.g:1922:1: ( 'with' )
            // InternalRSec.g:1923:2: 'with'
            {
             before(grammarAccess.getEventAccess().getWithKeyword_7()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getWithKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__7__Impl"


    // $ANTLR start "rule__Event__Group__8"
    // InternalRSec.g:1932:1: rule__Event__Group__8 : rule__Event__Group__8__Impl rule__Event__Group__9 ;
    public final void rule__Event__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1936:1: ( rule__Event__Group__8__Impl rule__Event__Group__9 )
            // InternalRSec.g:1937:2: rule__Event__Group__8__Impl rule__Event__Group__9
            {
            pushFollow(FOLLOW_27);
            rule__Event__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__8"


    // $ANTLR start "rule__Event__Group__8__Impl"
    // InternalRSec.g:1944:1: rule__Event__Group__8__Impl : ( ( rule__Event__VerbAssignment_8 ) ) ;
    public final void rule__Event__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1948:1: ( ( ( rule__Event__VerbAssignment_8 ) ) )
            // InternalRSec.g:1949:1: ( ( rule__Event__VerbAssignment_8 ) )
            {
            // InternalRSec.g:1949:1: ( ( rule__Event__VerbAssignment_8 ) )
            // InternalRSec.g:1950:2: ( rule__Event__VerbAssignment_8 )
            {
             before(grammarAccess.getEventAccess().getVerbAssignment_8()); 
            // InternalRSec.g:1951:2: ( rule__Event__VerbAssignment_8 )
            // InternalRSec.g:1951:3: rule__Event__VerbAssignment_8
            {
            pushFollow(FOLLOW_2);
            rule__Event__VerbAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getVerbAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__8__Impl"


    // $ANTLR start "rule__Event__Group__9"
    // InternalRSec.g:1959:1: rule__Event__Group__9 : rule__Event__Group__9__Impl rule__Event__Group__10 ;
    public final void rule__Event__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1963:1: ( rule__Event__Group__9__Impl rule__Event__Group__10 )
            // InternalRSec.g:1964:2: rule__Event__Group__9__Impl rule__Event__Group__10
            {
            pushFollow(FOLLOW_12);
            rule__Event__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__9"


    // $ANTLR start "rule__Event__Group__9__Impl"
    // InternalRSec.g:1971:1: rule__Event__Group__9__Impl : ( 'on' ) ;
    public final void rule__Event__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1975:1: ( ( 'on' ) )
            // InternalRSec.g:1976:1: ( 'on' )
            {
            // InternalRSec.g:1976:1: ( 'on' )
            // InternalRSec.g:1977:2: 'on'
            {
             before(grammarAccess.getEventAccess().getOnKeyword_9()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getOnKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__9__Impl"


    // $ANTLR start "rule__Event__Group__10"
    // InternalRSec.g:1986:1: rule__Event__Group__10 : rule__Event__Group__10__Impl rule__Event__Group__11 ;
    public final void rule__Event__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:1990:1: ( rule__Event__Group__10__Impl rule__Event__Group__11 )
            // InternalRSec.g:1991:2: rule__Event__Group__10__Impl rule__Event__Group__11
            {
            pushFollow(FOLLOW_28);
            rule__Event__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__10"


    // $ANTLR start "rule__Event__Group__10__Impl"
    // InternalRSec.g:1998:1: rule__Event__Group__10__Impl : ( ( rule__Event__PathAssignment_10 ) ) ;
    public final void rule__Event__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2002:1: ( ( ( rule__Event__PathAssignment_10 ) ) )
            // InternalRSec.g:2003:1: ( ( rule__Event__PathAssignment_10 ) )
            {
            // InternalRSec.g:2003:1: ( ( rule__Event__PathAssignment_10 ) )
            // InternalRSec.g:2004:2: ( rule__Event__PathAssignment_10 )
            {
             before(grammarAccess.getEventAccess().getPathAssignment_10()); 
            // InternalRSec.g:2005:2: ( rule__Event__PathAssignment_10 )
            // InternalRSec.g:2005:3: rule__Event__PathAssignment_10
            {
            pushFollow(FOLLOW_2);
            rule__Event__PathAssignment_10();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getPathAssignment_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__10__Impl"


    // $ANTLR start "rule__Event__Group__11"
    // InternalRSec.g:2013:1: rule__Event__Group__11 : rule__Event__Group__11__Impl rule__Event__Group__12 ;
    public final void rule__Event__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2017:1: ( rule__Event__Group__11__Impl rule__Event__Group__12 )
            // InternalRSec.g:2018:2: rule__Event__Group__11__Impl rule__Event__Group__12
            {
            pushFollow(FOLLOW_28);
            rule__Event__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__11"


    // $ANTLR start "rule__Event__Group__11__Impl"
    // InternalRSec.g:2025:1: rule__Event__Group__11__Impl : ( ( rule__Event__RolesAssignment_11 )? ) ;
    public final void rule__Event__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2029:1: ( ( ( rule__Event__RolesAssignment_11 )? ) )
            // InternalRSec.g:2030:1: ( ( rule__Event__RolesAssignment_11 )? )
            {
            // InternalRSec.g:2030:1: ( ( rule__Event__RolesAssignment_11 )? )
            // InternalRSec.g:2031:2: ( rule__Event__RolesAssignment_11 )?
            {
             before(grammarAccess.getEventAccess().getRolesAssignment_11()); 
            // InternalRSec.g:2032:2: ( rule__Event__RolesAssignment_11 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==37) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalRSec.g:2032:3: rule__Event__RolesAssignment_11
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__RolesAssignment_11();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getRolesAssignment_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__11__Impl"


    // $ANTLR start "rule__Event__Group__12"
    // InternalRSec.g:2040:1: rule__Event__Group__12 : rule__Event__Group__12__Impl ;
    public final void rule__Event__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2044:1: ( rule__Event__Group__12__Impl )
            // InternalRSec.g:2045:2: rule__Event__Group__12__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group__12__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__12"


    // $ANTLR start "rule__Event__Group__12__Impl"
    // InternalRSec.g:2051:1: rule__Event__Group__12__Impl : ( ( rule__Event__Group_12__0 )? ) ;
    public final void rule__Event__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2055:1: ( ( ( rule__Event__Group_12__0 )? ) )
            // InternalRSec.g:2056:1: ( ( rule__Event__Group_12__0 )? )
            {
            // InternalRSec.g:2056:1: ( ( rule__Event__Group_12__0 )? )
            // InternalRSec.g:2057:2: ( rule__Event__Group_12__0 )?
            {
             before(grammarAccess.getEventAccess().getGroup_12()); 
            // InternalRSec.g:2058:2: ( rule__Event__Group_12__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==32) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalRSec.g:2058:3: rule__Event__Group_12__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Event__Group_12__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEventAccess().getGroup_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group__12__Impl"


    // $ANTLR start "rule__Event__Group_12__0"
    // InternalRSec.g:2067:1: rule__Event__Group_12__0 : rule__Event__Group_12__0__Impl rule__Event__Group_12__1 ;
    public final void rule__Event__Group_12__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2071:1: ( rule__Event__Group_12__0__Impl rule__Event__Group_12__1 )
            // InternalRSec.g:2072:2: rule__Event__Group_12__0__Impl rule__Event__Group_12__1
            {
            pushFollow(FOLLOW_4);
            rule__Event__Group_12__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_12__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12__0"


    // $ANTLR start "rule__Event__Group_12__0__Impl"
    // InternalRSec.g:2079:1: rule__Event__Group_12__0__Impl : ( '[' ) ;
    public final void rule__Event__Group_12__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2083:1: ( ( '[' ) )
            // InternalRSec.g:2084:1: ( '[' )
            {
            // InternalRSec.g:2084:1: ( '[' )
            // InternalRSec.g:2085:2: '['
            {
             before(grammarAccess.getEventAccess().getLeftSquareBracketKeyword_12_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getLeftSquareBracketKeyword_12_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12__0__Impl"


    // $ANTLR start "rule__Event__Group_12__1"
    // InternalRSec.g:2094:1: rule__Event__Group_12__1 : rule__Event__Group_12__1__Impl rule__Event__Group_12__2 ;
    public final void rule__Event__Group_12__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2098:1: ( rule__Event__Group_12__1__Impl rule__Event__Group_12__2 )
            // InternalRSec.g:2099:2: rule__Event__Group_12__1__Impl rule__Event__Group_12__2
            {
            pushFollow(FOLLOW_29);
            rule__Event__Group_12__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_12__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12__1"


    // $ANTLR start "rule__Event__Group_12__1__Impl"
    // InternalRSec.g:2106:1: rule__Event__Group_12__1__Impl : ( ( rule__Event__MroleAssignment_12_1 ) ) ;
    public final void rule__Event__Group_12__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2110:1: ( ( ( rule__Event__MroleAssignment_12_1 ) ) )
            // InternalRSec.g:2111:1: ( ( rule__Event__MroleAssignment_12_1 ) )
            {
            // InternalRSec.g:2111:1: ( ( rule__Event__MroleAssignment_12_1 ) )
            // InternalRSec.g:2112:2: ( rule__Event__MroleAssignment_12_1 )
            {
             before(grammarAccess.getEventAccess().getMroleAssignment_12_1()); 
            // InternalRSec.g:2113:2: ( rule__Event__MroleAssignment_12_1 )
            // InternalRSec.g:2113:3: rule__Event__MroleAssignment_12_1
            {
            pushFollow(FOLLOW_2);
            rule__Event__MroleAssignment_12_1();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getMroleAssignment_12_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12__1__Impl"


    // $ANTLR start "rule__Event__Group_12__2"
    // InternalRSec.g:2121:1: rule__Event__Group_12__2 : rule__Event__Group_12__2__Impl rule__Event__Group_12__3 ;
    public final void rule__Event__Group_12__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2125:1: ( rule__Event__Group_12__2__Impl rule__Event__Group_12__3 )
            // InternalRSec.g:2126:2: rule__Event__Group_12__2__Impl rule__Event__Group_12__3
            {
            pushFollow(FOLLOW_29);
            rule__Event__Group_12__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_12__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12__2"


    // $ANTLR start "rule__Event__Group_12__2__Impl"
    // InternalRSec.g:2133:1: rule__Event__Group_12__2__Impl : ( ( rule__Event__Group_12_2__0 )* ) ;
    public final void rule__Event__Group_12__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2137:1: ( ( ( rule__Event__Group_12_2__0 )* ) )
            // InternalRSec.g:2138:1: ( ( rule__Event__Group_12_2__0 )* )
            {
            // InternalRSec.g:2138:1: ( ( rule__Event__Group_12_2__0 )* )
            // InternalRSec.g:2139:2: ( rule__Event__Group_12_2__0 )*
            {
             before(grammarAccess.getEventAccess().getGroup_12_2()); 
            // InternalRSec.g:2140:2: ( rule__Event__Group_12_2__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==26) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalRSec.g:2140:3: rule__Event__Group_12_2__0
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__Event__Group_12_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getEventAccess().getGroup_12_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12__2__Impl"


    // $ANTLR start "rule__Event__Group_12__3"
    // InternalRSec.g:2148:1: rule__Event__Group_12__3 : rule__Event__Group_12__3__Impl ;
    public final void rule__Event__Group_12__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2152:1: ( rule__Event__Group_12__3__Impl )
            // InternalRSec.g:2153:2: rule__Event__Group_12__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_12__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12__3"


    // $ANTLR start "rule__Event__Group_12__3__Impl"
    // InternalRSec.g:2159:1: rule__Event__Group_12__3__Impl : ( ']' ) ;
    public final void rule__Event__Group_12__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2163:1: ( ( ']' ) )
            // InternalRSec.g:2164:1: ( ']' )
            {
            // InternalRSec.g:2164:1: ( ']' )
            // InternalRSec.g:2165:2: ']'
            {
             before(grammarAccess.getEventAccess().getRightSquareBracketKeyword_12_3()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getRightSquareBracketKeyword_12_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12__3__Impl"


    // $ANTLR start "rule__Event__Group_12_2__0"
    // InternalRSec.g:2175:1: rule__Event__Group_12_2__0 : rule__Event__Group_12_2__0__Impl rule__Event__Group_12_2__1 ;
    public final void rule__Event__Group_12_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2179:1: ( rule__Event__Group_12_2__0__Impl rule__Event__Group_12_2__1 )
            // InternalRSec.g:2180:2: rule__Event__Group_12_2__0__Impl rule__Event__Group_12_2__1
            {
            pushFollow(FOLLOW_4);
            rule__Event__Group_12_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Event__Group_12_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12_2__0"


    // $ANTLR start "rule__Event__Group_12_2__0__Impl"
    // InternalRSec.g:2187:1: rule__Event__Group_12_2__0__Impl : ( ',' ) ;
    public final void rule__Event__Group_12_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2191:1: ( ( ',' ) )
            // InternalRSec.g:2192:1: ( ',' )
            {
            // InternalRSec.g:2192:1: ( ',' )
            // InternalRSec.g:2193:2: ','
            {
             before(grammarAccess.getEventAccess().getCommaKeyword_12_2_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getCommaKeyword_12_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12_2__0__Impl"


    // $ANTLR start "rule__Event__Group_12_2__1"
    // InternalRSec.g:2202:1: rule__Event__Group_12_2__1 : rule__Event__Group_12_2__1__Impl ;
    public final void rule__Event__Group_12_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2206:1: ( rule__Event__Group_12_2__1__Impl )
            // InternalRSec.g:2207:2: rule__Event__Group_12_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Event__Group_12_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12_2__1"


    // $ANTLR start "rule__Event__Group_12_2__1__Impl"
    // InternalRSec.g:2213:1: rule__Event__Group_12_2__1__Impl : ( ( rule__Event__MroleAssignment_12_2_1 ) ) ;
    public final void rule__Event__Group_12_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2217:1: ( ( ( rule__Event__MroleAssignment_12_2_1 ) ) )
            // InternalRSec.g:2218:1: ( ( rule__Event__MroleAssignment_12_2_1 ) )
            {
            // InternalRSec.g:2218:1: ( ( rule__Event__MroleAssignment_12_2_1 ) )
            // InternalRSec.g:2219:2: ( rule__Event__MroleAssignment_12_2_1 )
            {
             before(grammarAccess.getEventAccess().getMroleAssignment_12_2_1()); 
            // InternalRSec.g:2220:2: ( rule__Event__MroleAssignment_12_2_1 )
            // InternalRSec.g:2220:3: rule__Event__MroleAssignment_12_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Event__MroleAssignment_12_2_1();

            state._fsp--;


            }

             after(grammarAccess.getEventAccess().getMroleAssignment_12_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__Group_12_2__1__Impl"


    // $ANTLR start "rule__Property__Group__0"
    // InternalRSec.g:2229:1: rule__Property__Group__0 : rule__Property__Group__0__Impl rule__Property__Group__1 ;
    public final void rule__Property__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2233:1: ( rule__Property__Group__0__Impl rule__Property__Group__1 )
            // InternalRSec.g:2234:2: rule__Property__Group__0__Impl rule__Property__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__Property__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__0"


    // $ANTLR start "rule__Property__Group__0__Impl"
    // InternalRSec.g:2241:1: rule__Property__Group__0__Impl : ( ( rule__Property__NameAssignment_0 ) ) ;
    public final void rule__Property__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2245:1: ( ( ( rule__Property__NameAssignment_0 ) ) )
            // InternalRSec.g:2246:1: ( ( rule__Property__NameAssignment_0 ) )
            {
            // InternalRSec.g:2246:1: ( ( rule__Property__NameAssignment_0 ) )
            // InternalRSec.g:2247:2: ( rule__Property__NameAssignment_0 )
            {
             before(grammarAccess.getPropertyAccess().getNameAssignment_0()); 
            // InternalRSec.g:2248:2: ( rule__Property__NameAssignment_0 )
            // InternalRSec.g:2248:3: rule__Property__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Property__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__0__Impl"


    // $ANTLR start "rule__Property__Group__1"
    // InternalRSec.g:2256:1: rule__Property__Group__1 : rule__Property__Group__1__Impl rule__Property__Group__2 ;
    public final void rule__Property__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2260:1: ( rule__Property__Group__1__Impl rule__Property__Group__2 )
            // InternalRSec.g:2261:2: rule__Property__Group__1__Impl rule__Property__Group__2
            {
            pushFollow(FOLLOW_25);
            rule__Property__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__1"


    // $ANTLR start "rule__Property__Group__1__Impl"
    // InternalRSec.g:2268:1: rule__Property__Group__1__Impl : ( ':' ) ;
    public final void rule__Property__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2272:1: ( ( ':' ) )
            // InternalRSec.g:2273:1: ( ':' )
            {
            // InternalRSec.g:2273:1: ( ':' )
            // InternalRSec.g:2274:2: ':'
            {
             before(grammarAccess.getPropertyAccess().getColonKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__1__Impl"


    // $ANTLR start "rule__Property__Group__2"
    // InternalRSec.g:2283:1: rule__Property__Group__2 : rule__Property__Group__2__Impl rule__Property__Group__3 ;
    public final void rule__Property__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2287:1: ( rule__Property__Group__2__Impl rule__Property__Group__3 )
            // InternalRSec.g:2288:2: rule__Property__Group__2__Impl rule__Property__Group__3
            {
            pushFollow(FOLLOW_25);
            rule__Property__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__2"


    // $ANTLR start "rule__Property__Group__2__Impl"
    // InternalRSec.g:2295:1: rule__Property__Group__2__Impl : ( ( rule__Property__ManyAssignment_2 )? ) ;
    public final void rule__Property__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2299:1: ( ( ( rule__Property__ManyAssignment_2 )? ) )
            // InternalRSec.g:2300:1: ( ( rule__Property__ManyAssignment_2 )? )
            {
            // InternalRSec.g:2300:1: ( ( rule__Property__ManyAssignment_2 )? )
            // InternalRSec.g:2301:2: ( rule__Property__ManyAssignment_2 )?
            {
             before(grammarAccess.getPropertyAccess().getManyAssignment_2()); 
            // InternalRSec.g:2302:2: ( rule__Property__ManyAssignment_2 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==36) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalRSec.g:2302:3: rule__Property__ManyAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__ManyAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPropertyAccess().getManyAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__2__Impl"


    // $ANTLR start "rule__Property__Group__3"
    // InternalRSec.g:2310:1: rule__Property__Group__3 : rule__Property__Group__3__Impl rule__Property__Group__4 ;
    public final void rule__Property__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2314:1: ( rule__Property__Group__3__Impl rule__Property__Group__4 )
            // InternalRSec.g:2315:2: rule__Property__Group__3__Impl rule__Property__Group__4
            {
            pushFollow(FOLLOW_30);
            rule__Property__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Property__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__3"


    // $ANTLR start "rule__Property__Group__3__Impl"
    // InternalRSec.g:2322:1: rule__Property__Group__3__Impl : ( ( rule__Property__TypeAssignment_3 ) ) ;
    public final void rule__Property__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2326:1: ( ( ( rule__Property__TypeAssignment_3 ) ) )
            // InternalRSec.g:2327:1: ( ( rule__Property__TypeAssignment_3 ) )
            {
            // InternalRSec.g:2327:1: ( ( rule__Property__TypeAssignment_3 ) )
            // InternalRSec.g:2328:2: ( rule__Property__TypeAssignment_3 )
            {
             before(grammarAccess.getPropertyAccess().getTypeAssignment_3()); 
            // InternalRSec.g:2329:2: ( rule__Property__TypeAssignment_3 )
            // InternalRSec.g:2329:3: rule__Property__TypeAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Property__TypeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPropertyAccess().getTypeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__3__Impl"


    // $ANTLR start "rule__Property__Group__4"
    // InternalRSec.g:2337:1: rule__Property__Group__4 : rule__Property__Group__4__Impl ;
    public final void rule__Property__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2341:1: ( rule__Property__Group__4__Impl )
            // InternalRSec.g:2342:2: rule__Property__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Property__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__4"


    // $ANTLR start "rule__Property__Group__4__Impl"
    // InternalRSec.g:2348:1: rule__Property__Group__4__Impl : ( ( rule__Property__RenderAssignment_4 )? ) ;
    public final void rule__Property__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2352:1: ( ( ( rule__Property__RenderAssignment_4 )? ) )
            // InternalRSec.g:2353:1: ( ( rule__Property__RenderAssignment_4 )? )
            {
            // InternalRSec.g:2353:1: ( ( rule__Property__RenderAssignment_4 )? )
            // InternalRSec.g:2354:2: ( rule__Property__RenderAssignment_4 )?
            {
             before(grammarAccess.getPropertyAccess().getRenderAssignment_4()); 
            // InternalRSec.g:2355:2: ( rule__Property__RenderAssignment_4 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==38) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalRSec.g:2355:3: rule__Property__RenderAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Property__RenderAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPropertyAccess().getRenderAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__Group__4__Impl"


    // $ANTLR start "rule__State__Group__0"
    // InternalRSec.g:2364:1: rule__State__Group__0 : rule__State__Group__0__Impl rule__State__Group__1 ;
    public final void rule__State__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2368:1: ( rule__State__Group__0__Impl rule__State__Group__1 )
            // InternalRSec.g:2369:2: rule__State__Group__0__Impl rule__State__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__State__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0"


    // $ANTLR start "rule__State__Group__0__Impl"
    // InternalRSec.g:2376:1: rule__State__Group__0__Impl : ( 'state' ) ;
    public final void rule__State__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2380:1: ( ( 'state' ) )
            // InternalRSec.g:2381:1: ( 'state' )
            {
            // InternalRSec.g:2381:1: ( 'state' )
            // InternalRSec.g:2382:2: 'state'
            {
             before(grammarAccess.getStateAccess().getStateKeyword_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getStateKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__0__Impl"


    // $ANTLR start "rule__State__Group__1"
    // InternalRSec.g:2391:1: rule__State__Group__1 : rule__State__Group__1__Impl rule__State__Group__2 ;
    public final void rule__State__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2395:1: ( rule__State__Group__1__Impl rule__State__Group__2 )
            // InternalRSec.g:2396:2: rule__State__Group__1__Impl rule__State__Group__2
            {
            pushFollow(FOLLOW_31);
            rule__State__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1"


    // $ANTLR start "rule__State__Group__1__Impl"
    // InternalRSec.g:2403:1: rule__State__Group__1__Impl : ( ( rule__State__NameAssignment_1 ) ) ;
    public final void rule__State__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2407:1: ( ( ( rule__State__NameAssignment_1 ) ) )
            // InternalRSec.g:2408:1: ( ( rule__State__NameAssignment_1 ) )
            {
            // InternalRSec.g:2408:1: ( ( rule__State__NameAssignment_1 ) )
            // InternalRSec.g:2409:2: ( rule__State__NameAssignment_1 )
            {
             before(grammarAccess.getStateAccess().getNameAssignment_1()); 
            // InternalRSec.g:2410:2: ( rule__State__NameAssignment_1 )
            // InternalRSec.g:2410:3: rule__State__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__State__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStateAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__1__Impl"


    // $ANTLR start "rule__State__Group__2"
    // InternalRSec.g:2418:1: rule__State__Group__2 : rule__State__Group__2__Impl rule__State__Group__3 ;
    public final void rule__State__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2422:1: ( rule__State__Group__2__Impl rule__State__Group__3 )
            // InternalRSec.g:2423:2: rule__State__Group__2__Impl rule__State__Group__3
            {
            pushFollow(FOLLOW_31);
            rule__State__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__State__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__2"


    // $ANTLR start "rule__State__Group__2__Impl"
    // InternalRSec.g:2430:1: rule__State__Group__2__Impl : ( ( rule__State__TransitionsAssignment_2 )* ) ;
    public final void rule__State__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2434:1: ( ( ( rule__State__TransitionsAssignment_2 )* ) )
            // InternalRSec.g:2435:1: ( ( rule__State__TransitionsAssignment_2 )* )
            {
            // InternalRSec.g:2435:1: ( ( rule__State__TransitionsAssignment_2 )* )
            // InternalRSec.g:2436:2: ( rule__State__TransitionsAssignment_2 )*
            {
             before(grammarAccess.getStateAccess().getTransitionsAssignment_2()); 
            // InternalRSec.g:2437:2: ( rule__State__TransitionsAssignment_2 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==RULE_ID) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalRSec.g:2437:3: rule__State__TransitionsAssignment_2
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__State__TransitionsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getStateAccess().getTransitionsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__2__Impl"


    // $ANTLR start "rule__State__Group__3"
    // InternalRSec.g:2445:1: rule__State__Group__3 : rule__State__Group__3__Impl ;
    public final void rule__State__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2449:1: ( rule__State__Group__3__Impl )
            // InternalRSec.g:2450:2: rule__State__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__State__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__3"


    // $ANTLR start "rule__State__Group__3__Impl"
    // InternalRSec.g:2456:1: rule__State__Group__3__Impl : ( 'end' ) ;
    public final void rule__State__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2460:1: ( ( 'end' ) )
            // InternalRSec.g:2461:1: ( 'end' )
            {
            // InternalRSec.g:2461:1: ( 'end' )
            // InternalRSec.g:2462:2: 'end'
            {
             before(grammarAccess.getStateAccess().getEndKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getStateAccess().getEndKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__Group__3__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // InternalRSec.g:2472:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2476:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // InternalRSec.g:2477:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FOLLOW_32);
            rule__Transition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // InternalRSec.g:2484:1: rule__Transition__Group__0__Impl : ( ( rule__Transition__EventAssignment_0 ) ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2488:1: ( ( ( rule__Transition__EventAssignment_0 ) ) )
            // InternalRSec.g:2489:1: ( ( rule__Transition__EventAssignment_0 ) )
            {
            // InternalRSec.g:2489:1: ( ( rule__Transition__EventAssignment_0 ) )
            // InternalRSec.g:2490:2: ( rule__Transition__EventAssignment_0 )
            {
             before(grammarAccess.getTransitionAccess().getEventAssignment_0()); 
            // InternalRSec.g:2491:2: ( rule__Transition__EventAssignment_0 )
            // InternalRSec.g:2491:3: rule__Transition__EventAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Transition__EventAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getEventAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // InternalRSec.g:2499:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2503:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // InternalRSec.g:2504:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Transition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Transition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // InternalRSec.g:2511:1: rule__Transition__Group__1__Impl : ( '=>' ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2515:1: ( ( '=>' ) )
            // InternalRSec.g:2516:1: ( '=>' )
            {
            // InternalRSec.g:2516:1: ( '=>' )
            // InternalRSec.g:2517:2: '=>'
            {
             before(grammarAccess.getTransitionAccess().getEqualsSignGreaterThanSignKeyword_1()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getEqualsSignGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // InternalRSec.g:2526:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2530:1: ( rule__Transition__Group__2__Impl )
            // InternalRSec.g:2531:2: rule__Transition__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Transition__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // InternalRSec.g:2537:1: rule__Transition__Group__2__Impl : ( ( rule__Transition__StateAssignment_2 ) ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2541:1: ( ( ( rule__Transition__StateAssignment_2 ) ) )
            // InternalRSec.g:2542:1: ( ( rule__Transition__StateAssignment_2 ) )
            {
            // InternalRSec.g:2542:1: ( ( rule__Transition__StateAssignment_2 ) )
            // InternalRSec.g:2543:2: ( rule__Transition__StateAssignment_2 )
            {
             before(grammarAccess.getTransitionAccess().getStateAssignment_2()); 
            // InternalRSec.g:2544:2: ( rule__Transition__StateAssignment_2 )
            // InternalRSec.g:2544:3: rule__Transition__StateAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Transition__StateAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getTransitionAccess().getStateAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__ResourceSpecification__PackageNameAssignment_2"
    // InternalRSec.g:2553:1: rule__ResourceSpecification__PackageNameAssignment_2 : ( ruleQualifiedName ) ;
    public final void rule__ResourceSpecification__PackageNameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2557:1: ( ( ruleQualifiedName ) )
            // InternalRSec.g:2558:2: ( ruleQualifiedName )
            {
            // InternalRSec.g:2558:2: ( ruleQualifiedName )
            // InternalRSec.g:2559:3: ruleQualifiedName
            {
             before(grammarAccess.getResourceSpecificationAccess().getPackageNameQualifiedNameParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getResourceSpecificationAccess().getPackageNameQualifiedNameParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__PackageNameAssignment_2"


    // $ANTLR start "rule__ResourceSpecification__ImportsAssignment_3"
    // InternalRSec.g:2568:1: rule__ResourceSpecification__ImportsAssignment_3 : ( ruleImport ) ;
    public final void rule__ResourceSpecification__ImportsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2572:1: ( ( ruleImport ) )
            // InternalRSec.g:2573:2: ( ruleImport )
            {
            // InternalRSec.g:2573:2: ( ruleImport )
            // InternalRSec.g:2574:3: ruleImport
            {
             before(grammarAccess.getResourceSpecificationAccess().getImportsImportParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getResourceSpecificationAccess().getImportsImportParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__ImportsAssignment_3"


    // $ANTLR start "rule__ResourceSpecification__ElementsAssignment_4"
    // InternalRSec.g:2583:1: rule__ResourceSpecification__ElementsAssignment_4 : ( ruleType ) ;
    public final void rule__ResourceSpecification__ElementsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2587:1: ( ( ruleType ) )
            // InternalRSec.g:2588:2: ( ruleType )
            {
            // InternalRSec.g:2588:2: ( ruleType )
            // InternalRSec.g:2589:3: ruleType
            {
             before(grammarAccess.getResourceSpecificationAccess().getElementsTypeParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getResourceSpecificationAccess().getElementsTypeParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceSpecification__ElementsAssignment_4"


    // $ANTLR start "rule__DataType__NameAssignment_1"
    // InternalRSec.g:2598:1: rule__DataType__NameAssignment_1 : ( ruleQualifiedName ) ;
    public final void rule__DataType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2602:1: ( ( ruleQualifiedName ) )
            // InternalRSec.g:2603:2: ( ruleQualifiedName )
            {
            // InternalRSec.g:2603:2: ( ruleQualifiedName )
            // InternalRSec.g:2604:3: ruleQualifiedName
            {
             before(grammarAccess.getDataTypeAccess().getNameQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getDataTypeAccess().getNameQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataType__NameAssignment_1"


    // $ANTLR start "rule__Import__ImportedNamespaceAssignment_1"
    // InternalRSec.g:2613:1: rule__Import__ImportedNamespaceAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__Import__ImportedNamespaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2617:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalRSec.g:2618:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalRSec.g:2618:2: ( ruleQualifiedNameWithWildcard )
            // InternalRSec.g:2619:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportedNamespaceAssignment_1"


    // $ANTLR start "rule__Roles__NameAssignment_1"
    // InternalRSec.g:2628:1: rule__Roles__NameAssignment_1 : ( ruleQualifiedName ) ;
    public final void rule__Roles__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2632:1: ( ( ruleQualifiedName ) )
            // InternalRSec.g:2633:2: ( ruleQualifiedName )
            {
            // InternalRSec.g:2633:2: ( ruleQualifiedName )
            // InternalRSec.g:2634:3: ruleQualifiedName
            {
             before(grammarAccess.getRolesAccess().getNameQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getRolesAccess().getNameQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Roles__NameAssignment_1"


    // $ANTLR start "rule__ResourceType__NameAssignment_1"
    // InternalRSec.g:2643:1: rule__ResourceType__NameAssignment_1 : ( ruleQualifiedName ) ;
    public final void rule__ResourceType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2647:1: ( ( ruleQualifiedName ) )
            // InternalRSec.g:2648:2: ( ruleQualifiedName )
            {
            // InternalRSec.g:2648:2: ( ruleQualifiedName )
            // InternalRSec.g:2649:3: ruleQualifiedName
            {
             before(grammarAccess.getResourceTypeAccess().getNameQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getResourceTypeAccess().getNameQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__NameAssignment_1"


    // $ANTLR start "rule__ResourceType__PathAssignment_2_1"
    // InternalRSec.g:2658:1: rule__ResourceType__PathAssignment_2_1 : ( RULE_STRING ) ;
    public final void rule__ResourceType__PathAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2662:1: ( ( RULE_STRING ) )
            // InternalRSec.g:2663:2: ( RULE_STRING )
            {
            // InternalRSec.g:2663:2: ( RULE_STRING )
            // InternalRSec.g:2664:3: RULE_STRING
            {
             before(grammarAccess.getResourceTypeAccess().getPathSTRINGTerminalRuleCall_2_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getPathSTRINGTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__PathAssignment_2_1"


    // $ANTLR start "rule__ResourceType__ViewsAssignment_3_1"
    // InternalRSec.g:2673:1: rule__ResourceType__ViewsAssignment_3_1 : ( RULE_STRING ) ;
    public final void rule__ResourceType__ViewsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2677:1: ( ( RULE_STRING ) )
            // InternalRSec.g:2678:2: ( RULE_STRING )
            {
            // InternalRSec.g:2678:2: ( RULE_STRING )
            // InternalRSec.g:2679:3: RULE_STRING
            {
             before(grammarAccess.getResourceTypeAccess().getViewsSTRINGTerminalRuleCall_3_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getViewsSTRINGTerminalRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__ViewsAssignment_3_1"


    // $ANTLR start "rule__ResourceType__PropertiesAssignment_4_1"
    // InternalRSec.g:2688:1: rule__ResourceType__PropertiesAssignment_4_1 : ( ruleProperty ) ;
    public final void rule__ResourceType__PropertiesAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2692:1: ( ( ruleProperty ) )
            // InternalRSec.g:2693:2: ( ruleProperty )
            {
            // InternalRSec.g:2693:2: ( ruleProperty )
            // InternalRSec.g:2694:3: ruleProperty
            {
             before(grammarAccess.getResourceTypeAccess().getPropertiesPropertyParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleProperty();

            state._fsp--;

             after(grammarAccess.getResourceTypeAccess().getPropertiesPropertyParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__PropertiesAssignment_4_1"


    // $ANTLR start "rule__ResourceType__PathvariableAssignment_5_1_1"
    // InternalRSec.g:2703:1: rule__ResourceType__PathvariableAssignment_5_1_1 : ( RULE_STRING ) ;
    public final void rule__ResourceType__PathvariableAssignment_5_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2707:1: ( ( RULE_STRING ) )
            // InternalRSec.g:2708:2: ( RULE_STRING )
            {
            // InternalRSec.g:2708:2: ( RULE_STRING )
            // InternalRSec.g:2709:3: RULE_STRING
            {
             before(grammarAccess.getResourceTypeAccess().getPathvariableSTRINGTerminalRuleCall_5_1_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getPathvariableSTRINGTerminalRuleCall_5_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__PathvariableAssignment_5_1_1"


    // $ANTLR start "rule__ResourceType__ResourceAssignment_5_1_3"
    // InternalRSec.g:2718:1: rule__ResourceType__ResourceAssignment_5_1_3 : ( ( RULE_ID ) ) ;
    public final void rule__ResourceType__ResourceAssignment_5_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2722:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:2723:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:2723:2: ( ( RULE_ID ) )
            // InternalRSec.g:2724:3: ( RULE_ID )
            {
             before(grammarAccess.getResourceTypeAccess().getResourceResourceTypeCrossReference_5_1_3_0()); 
            // InternalRSec.g:2725:3: ( RULE_ID )
            // InternalRSec.g:2726:4: RULE_ID
            {
             before(grammarAccess.getResourceTypeAccess().getResourceResourceTypeIDTerminalRuleCall_5_1_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getResourceResourceTypeIDTerminalRuleCall_5_1_3_0_1()); 

            }

             after(grammarAccess.getResourceTypeAccess().getResourceResourceTypeCrossReference_5_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__ResourceAssignment_5_1_3"


    // $ANTLR start "rule__ResourceType__PathvariableAssignment_5_1_4_1"
    // InternalRSec.g:2737:1: rule__ResourceType__PathvariableAssignment_5_1_4_1 : ( RULE_STRING ) ;
    public final void rule__ResourceType__PathvariableAssignment_5_1_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2741:1: ( ( RULE_STRING ) )
            // InternalRSec.g:2742:2: ( RULE_STRING )
            {
            // InternalRSec.g:2742:2: ( RULE_STRING )
            // InternalRSec.g:2743:3: RULE_STRING
            {
             before(grammarAccess.getResourceTypeAccess().getPathvariableSTRINGTerminalRuleCall_5_1_4_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getPathvariableSTRINGTerminalRuleCall_5_1_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__PathvariableAssignment_5_1_4_1"


    // $ANTLR start "rule__ResourceType__ResourceAssignment_5_1_4_3"
    // InternalRSec.g:2752:1: rule__ResourceType__ResourceAssignment_5_1_4_3 : ( ( RULE_ID ) ) ;
    public final void rule__ResourceType__ResourceAssignment_5_1_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2756:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:2757:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:2757:2: ( ( RULE_ID ) )
            // InternalRSec.g:2758:3: ( RULE_ID )
            {
             before(grammarAccess.getResourceTypeAccess().getResourceResourceTypeCrossReference_5_1_4_3_0()); 
            // InternalRSec.g:2759:3: ( RULE_ID )
            // InternalRSec.g:2760:4: RULE_ID
            {
             before(grammarAccess.getResourceTypeAccess().getResourceResourceTypeIDTerminalRuleCall_5_1_4_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getResourceTypeAccess().getResourceResourceTypeIDTerminalRuleCall_5_1_4_3_0_1()); 

            }

             after(grammarAccess.getResourceTypeAccess().getResourceResourceTypeCrossReference_5_1_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__ResourceAssignment_5_1_4_3"


    // $ANTLR start "rule__ResourceType__EventsAssignment_5_2"
    // InternalRSec.g:2771:1: rule__ResourceType__EventsAssignment_5_2 : ( ruleEvent ) ;
    public final void rule__ResourceType__EventsAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2775:1: ( ( ruleEvent ) )
            // InternalRSec.g:2776:2: ( ruleEvent )
            {
            // InternalRSec.g:2776:2: ( ruleEvent )
            // InternalRSec.g:2777:3: ruleEvent
            {
             before(grammarAccess.getResourceTypeAccess().getEventsEventParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEvent();

            state._fsp--;

             after(grammarAccess.getResourceTypeAccess().getEventsEventParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__EventsAssignment_5_2"


    // $ANTLR start "rule__ResourceType__StatesAssignment_6_1"
    // InternalRSec.g:2786:1: rule__ResourceType__StatesAssignment_6_1 : ( ruleState ) ;
    public final void rule__ResourceType__StatesAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2790:1: ( ( ruleState ) )
            // InternalRSec.g:2791:2: ( ruleState )
            {
            // InternalRSec.g:2791:2: ( ruleState )
            // InternalRSec.g:2792:3: ruleState
            {
             before(grammarAccess.getResourceTypeAccess().getStatesStateParserRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleState();

            state._fsp--;

             after(grammarAccess.getResourceTypeAccess().getStatesStateParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResourceType__StatesAssignment_6_1"


    // $ANTLR start "rule__Event__NameAssignment_0"
    // InternalRSec.g:2801:1: rule__Event__NameAssignment_0 : ( ruleQualifiedName ) ;
    public final void rule__Event__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2805:1: ( ( ruleQualifiedName ) )
            // InternalRSec.g:2806:2: ( ruleQualifiedName )
            {
            // InternalRSec.g:2806:2: ( ruleQualifiedName )
            // InternalRSec.g:2807:3: ruleQualifiedName
            {
             before(grammarAccess.getEventAccess().getNameQualifiedNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getEventAccess().getNameQualifiedNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__NameAssignment_0"


    // $ANTLR start "rule__Event__ParamTypeAssignment_2"
    // InternalRSec.g:2816:1: rule__Event__ParamTypeAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__Event__ParamTypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2820:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:2821:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:2821:2: ( ( RULE_ID ) )
            // InternalRSec.g:2822:3: ( RULE_ID )
            {
             before(grammarAccess.getEventAccess().getParamTypeResourceTypeCrossReference_2_0()); 
            // InternalRSec.g:2823:3: ( RULE_ID )
            // InternalRSec.g:2824:4: RULE_ID
            {
             before(grammarAccess.getEventAccess().getParamTypeResourceTypeIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getParamTypeResourceTypeIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getEventAccess().getParamTypeResourceTypeCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__ParamTypeAssignment_2"


    // $ANTLR start "rule__Event__ManyAssignment_5"
    // InternalRSec.g:2835:1: rule__Event__ManyAssignment_5 : ( ( 'many' ) ) ;
    public final void rule__Event__ManyAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2839:1: ( ( ( 'many' ) ) )
            // InternalRSec.g:2840:2: ( ( 'many' ) )
            {
            // InternalRSec.g:2840:2: ( ( 'many' ) )
            // InternalRSec.g:2841:3: ( 'many' )
            {
             before(grammarAccess.getEventAccess().getManyManyKeyword_5_0()); 
            // InternalRSec.g:2842:3: ( 'many' )
            // InternalRSec.g:2843:4: 'many'
            {
             before(grammarAccess.getEventAccess().getManyManyKeyword_5_0()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getManyManyKeyword_5_0()); 

            }

             after(grammarAccess.getEventAccess().getManyManyKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__ManyAssignment_5"


    // $ANTLR start "rule__Event__ReturnTypeAssignment_6"
    // InternalRSec.g:2854:1: rule__Event__ReturnTypeAssignment_6 : ( ( RULE_ID ) ) ;
    public final void rule__Event__ReturnTypeAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2858:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:2859:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:2859:2: ( ( RULE_ID ) )
            // InternalRSec.g:2860:3: ( RULE_ID )
            {
             before(grammarAccess.getEventAccess().getReturnTypeTypeCrossReference_6_0()); 
            // InternalRSec.g:2861:3: ( RULE_ID )
            // InternalRSec.g:2862:4: RULE_ID
            {
             before(grammarAccess.getEventAccess().getReturnTypeTypeIDTerminalRuleCall_6_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getReturnTypeTypeIDTerminalRuleCall_6_0_1()); 

            }

             after(grammarAccess.getEventAccess().getReturnTypeTypeCrossReference_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__ReturnTypeAssignment_6"


    // $ANTLR start "rule__Event__VerbAssignment_8"
    // InternalRSec.g:2873:1: rule__Event__VerbAssignment_8 : ( RULE_ID ) ;
    public final void rule__Event__VerbAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2877:1: ( ( RULE_ID ) )
            // InternalRSec.g:2878:2: ( RULE_ID )
            {
            // InternalRSec.g:2878:2: ( RULE_ID )
            // InternalRSec.g:2879:3: RULE_ID
            {
             before(grammarAccess.getEventAccess().getVerbIDTerminalRuleCall_8_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getVerbIDTerminalRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__VerbAssignment_8"


    // $ANTLR start "rule__Event__PathAssignment_10"
    // InternalRSec.g:2888:1: rule__Event__PathAssignment_10 : ( RULE_STRING ) ;
    public final void rule__Event__PathAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2892:1: ( ( RULE_STRING ) )
            // InternalRSec.g:2893:2: ( RULE_STRING )
            {
            // InternalRSec.g:2893:2: ( RULE_STRING )
            // InternalRSec.g:2894:3: RULE_STRING
            {
             before(grammarAccess.getEventAccess().getPathSTRINGTerminalRuleCall_10_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getPathSTRINGTerminalRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__PathAssignment_10"


    // $ANTLR start "rule__Event__RolesAssignment_11"
    // InternalRSec.g:2903:1: rule__Event__RolesAssignment_11 : ( ( 'roles' ) ) ;
    public final void rule__Event__RolesAssignment_11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2907:1: ( ( ( 'roles' ) ) )
            // InternalRSec.g:2908:2: ( ( 'roles' ) )
            {
            // InternalRSec.g:2908:2: ( ( 'roles' ) )
            // InternalRSec.g:2909:3: ( 'roles' )
            {
             before(grammarAccess.getEventAccess().getRolesRolesKeyword_11_0()); 
            // InternalRSec.g:2910:3: ( 'roles' )
            // InternalRSec.g:2911:4: 'roles'
            {
             before(grammarAccess.getEventAccess().getRolesRolesKeyword_11_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getRolesRolesKeyword_11_0()); 

            }

             after(grammarAccess.getEventAccess().getRolesRolesKeyword_11_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__RolesAssignment_11"


    // $ANTLR start "rule__Event__MroleAssignment_12_1"
    // InternalRSec.g:2922:1: rule__Event__MroleAssignment_12_1 : ( ( RULE_ID ) ) ;
    public final void rule__Event__MroleAssignment_12_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2926:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:2927:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:2927:2: ( ( RULE_ID ) )
            // InternalRSec.g:2928:3: ( RULE_ID )
            {
             before(grammarAccess.getEventAccess().getMroleRolesCrossReference_12_1_0()); 
            // InternalRSec.g:2929:3: ( RULE_ID )
            // InternalRSec.g:2930:4: RULE_ID
            {
             before(grammarAccess.getEventAccess().getMroleRolesIDTerminalRuleCall_12_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getMroleRolesIDTerminalRuleCall_12_1_0_1()); 

            }

             after(grammarAccess.getEventAccess().getMroleRolesCrossReference_12_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__MroleAssignment_12_1"


    // $ANTLR start "rule__Event__MroleAssignment_12_2_1"
    // InternalRSec.g:2941:1: rule__Event__MroleAssignment_12_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__Event__MroleAssignment_12_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2945:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:2946:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:2946:2: ( ( RULE_ID ) )
            // InternalRSec.g:2947:3: ( RULE_ID )
            {
             before(grammarAccess.getEventAccess().getMroleRolesCrossReference_12_2_1_0()); 
            // InternalRSec.g:2948:3: ( RULE_ID )
            // InternalRSec.g:2949:4: RULE_ID
            {
             before(grammarAccess.getEventAccess().getMroleRolesIDTerminalRuleCall_12_2_1_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEventAccess().getMroleRolesIDTerminalRuleCall_12_2_1_0_1()); 

            }

             after(grammarAccess.getEventAccess().getMroleRolesCrossReference_12_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__MroleAssignment_12_2_1"


    // $ANTLR start "rule__Property__NameAssignment_0"
    // InternalRSec.g:2960:1: rule__Property__NameAssignment_0 : ( ruleQualifiedName ) ;
    public final void rule__Property__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2964:1: ( ( ruleQualifiedName ) )
            // InternalRSec.g:2965:2: ( ruleQualifiedName )
            {
            // InternalRSec.g:2965:2: ( ruleQualifiedName )
            // InternalRSec.g:2966:3: ruleQualifiedName
            {
             before(grammarAccess.getPropertyAccess().getNameQualifiedNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getPropertyAccess().getNameQualifiedNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__NameAssignment_0"


    // $ANTLR start "rule__Property__ManyAssignment_2"
    // InternalRSec.g:2975:1: rule__Property__ManyAssignment_2 : ( ( 'many' ) ) ;
    public final void rule__Property__ManyAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2979:1: ( ( ( 'many' ) ) )
            // InternalRSec.g:2980:2: ( ( 'many' ) )
            {
            // InternalRSec.g:2980:2: ( ( 'many' ) )
            // InternalRSec.g:2981:3: ( 'many' )
            {
             before(grammarAccess.getPropertyAccess().getManyManyKeyword_2_0()); 
            // InternalRSec.g:2982:3: ( 'many' )
            // InternalRSec.g:2983:4: 'many'
            {
             before(grammarAccess.getPropertyAccess().getManyManyKeyword_2_0()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getManyManyKeyword_2_0()); 

            }

             after(grammarAccess.getPropertyAccess().getManyManyKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__ManyAssignment_2"


    // $ANTLR start "rule__Property__TypeAssignment_3"
    // InternalRSec.g:2994:1: rule__Property__TypeAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__Property__TypeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:2998:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:2999:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:2999:2: ( ( RULE_ID ) )
            // InternalRSec.g:3000:3: ( RULE_ID )
            {
             before(grammarAccess.getPropertyAccess().getTypeTypeCrossReference_3_0()); 
            // InternalRSec.g:3001:3: ( RULE_ID )
            // InternalRSec.g:3002:4: RULE_ID
            {
             before(grammarAccess.getPropertyAccess().getTypeTypeIDTerminalRuleCall_3_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getTypeTypeIDTerminalRuleCall_3_0_1()); 

            }

             after(grammarAccess.getPropertyAccess().getTypeTypeCrossReference_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__TypeAssignment_3"


    // $ANTLR start "rule__Property__RenderAssignment_4"
    // InternalRSec.g:3013:1: rule__Property__RenderAssignment_4 : ( ( 'rendered' ) ) ;
    public final void rule__Property__RenderAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:3017:1: ( ( ( 'rendered' ) ) )
            // InternalRSec.g:3018:2: ( ( 'rendered' ) )
            {
            // InternalRSec.g:3018:2: ( ( 'rendered' ) )
            // InternalRSec.g:3019:3: ( 'rendered' )
            {
             before(grammarAccess.getPropertyAccess().getRenderRenderedKeyword_4_0()); 
            // InternalRSec.g:3020:3: ( 'rendered' )
            // InternalRSec.g:3021:4: 'rendered'
            {
             before(grammarAccess.getPropertyAccess().getRenderRenderedKeyword_4_0()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getPropertyAccess().getRenderRenderedKeyword_4_0()); 

            }

             after(grammarAccess.getPropertyAccess().getRenderRenderedKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Property__RenderAssignment_4"


    // $ANTLR start "rule__State__NameAssignment_1"
    // InternalRSec.g:3032:1: rule__State__NameAssignment_1 : ( ruleQualifiedName ) ;
    public final void rule__State__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:3036:1: ( ( ruleQualifiedName ) )
            // InternalRSec.g:3037:2: ( ruleQualifiedName )
            {
            // InternalRSec.g:3037:2: ( ruleQualifiedName )
            // InternalRSec.g:3038:3: ruleQualifiedName
            {
             before(grammarAccess.getStateAccess().getNameQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getStateAccess().getNameQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__NameAssignment_1"


    // $ANTLR start "rule__State__TransitionsAssignment_2"
    // InternalRSec.g:3047:1: rule__State__TransitionsAssignment_2 : ( ruleTransition ) ;
    public final void rule__State__TransitionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:3051:1: ( ( ruleTransition ) )
            // InternalRSec.g:3052:2: ( ruleTransition )
            {
            // InternalRSec.g:3052:2: ( ruleTransition )
            // InternalRSec.g:3053:3: ruleTransition
            {
             before(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTransition();

            state._fsp--;

             after(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__State__TransitionsAssignment_2"


    // $ANTLR start "rule__Transition__EventAssignment_0"
    // InternalRSec.g:3062:1: rule__Transition__EventAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__EventAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:3066:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:3067:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:3067:2: ( ( RULE_ID ) )
            // InternalRSec.g:3068:3: ( RULE_ID )
            {
             before(grammarAccess.getTransitionAccess().getEventEventCrossReference_0_0()); 
            // InternalRSec.g:3069:3: ( RULE_ID )
            // InternalRSec.g:3070:4: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getEventEventIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getEventEventIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getEventEventCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__EventAssignment_0"


    // $ANTLR start "rule__Transition__StateAssignment_2"
    // InternalRSec.g:3081:1: rule__Transition__StateAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__StateAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalRSec.g:3085:1: ( ( ( RULE_ID ) ) )
            // InternalRSec.g:3086:2: ( ( RULE_ID ) )
            {
            // InternalRSec.g:3086:2: ( ( RULE_ID ) )
            // InternalRSec.g:3087:3: ( RULE_ID )
            {
             before(grammarAccess.getTransitionAccess().getStateStateCrossReference_2_0()); 
            // InternalRSec.g:3088:3: ( RULE_ID )
            // InternalRSec.g:3089:4: RULE_ID
            {
             before(grammarAccess.getTransitionAccess().getStateStateIDTerminalRuleCall_2_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getTransitionAccess().getStateStateIDTerminalRuleCall_2_0_1()); 

            }

             after(grammarAccess.getTransitionAccess().getStateStateCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__StateAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000033000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000031002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000000087C0000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000800012L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000006000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000400040000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000020000010L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000001000000010L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000002100000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000204000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000040010L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000800000000L});

}
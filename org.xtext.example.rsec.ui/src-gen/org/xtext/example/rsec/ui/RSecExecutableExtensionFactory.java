/*
 * generated by Xtext 2.9.1
 */
package org.xtext.example.rsec.ui;

import com.google.inject.Injector;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;
import org.xtext.example.rsec.ui.internal.RsecActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class RSecExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return RsecActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return RsecActivator.getInstance().getInjector(RsecActivator.ORG_XTEXT_EXAMPLE_RSEC_RSEC);
	}
	
}

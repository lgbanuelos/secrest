/**
 * generated by Xtext 2.9.1
 */
package org.xtext.example.rsec.rSec;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.rsec.rSec.Event#getName <em>Name</em>}</li>
 *   <li>{@link org.xtext.example.rsec.rSec.Event#getParamType <em>Param Type</em>}</li>
 *   <li>{@link org.xtext.example.rsec.rSec.Event#isMany <em>Many</em>}</li>
 *   <li>{@link org.xtext.example.rsec.rSec.Event#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link org.xtext.example.rsec.rSec.Event#getVerb <em>Verb</em>}</li>
 *   <li>{@link org.xtext.example.rsec.rSec.Event#getPath <em>Path</em>}</li>
 *   <li>{@link org.xtext.example.rsec.rSec.Event#isRoles <em>Roles</em>}</li>
 *   <li>{@link org.xtext.example.rsec.rSec.Event#getMrole <em>Mrole</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link org.xtext.example.rsec.rSec.Event#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Param Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Param Type</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param Type</em>' reference.
   * @see #setParamType(ResourceType)
   * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent_ParamType()
   * @model
   * @generated
   */
  ResourceType getParamType();

  /**
   * Sets the value of the '{@link org.xtext.example.rsec.rSec.Event#getParamType <em>Param Type</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param Type</em>' reference.
   * @see #getParamType()
   * @generated
   */
  void setParamType(ResourceType value);

  /**
   * Returns the value of the '<em><b>Many</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Many</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Many</em>' attribute.
   * @see #setMany(boolean)
   * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent_Many()
   * @model
   * @generated
   */
  boolean isMany();

  /**
   * Sets the value of the '{@link org.xtext.example.rsec.rSec.Event#isMany <em>Many</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Many</em>' attribute.
   * @see #isMany()
   * @generated
   */
  void setMany(boolean value);

  /**
   * Returns the value of the '<em><b>Return Type</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return Type</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return Type</em>' reference.
   * @see #setReturnType(Type)
   * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent_ReturnType()
   * @model
   * @generated
   */
  Type getReturnType();

  /**
   * Sets the value of the '{@link org.xtext.example.rsec.rSec.Event#getReturnType <em>Return Type</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return Type</em>' reference.
   * @see #getReturnType()
   * @generated
   */
  void setReturnType(Type value);

  /**
   * Returns the value of the '<em><b>Verb</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Verb</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Verb</em>' attribute.
   * @see #setVerb(String)
   * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent_Verb()
   * @model
   * @generated
   */
  String getVerb();

  /**
   * Sets the value of the '{@link org.xtext.example.rsec.rSec.Event#getVerb <em>Verb</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Verb</em>' attribute.
   * @see #getVerb()
   * @generated
   */
  void setVerb(String value);

  /**
   * Returns the value of the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Path</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Path</em>' attribute.
   * @see #setPath(String)
   * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent_Path()
   * @model
   * @generated
   */
  String getPath();

  /**
   * Sets the value of the '{@link org.xtext.example.rsec.rSec.Event#getPath <em>Path</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Path</em>' attribute.
   * @see #getPath()
   * @generated
   */
  void setPath(String value);

  /**
   * Returns the value of the '<em><b>Roles</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Roles</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Roles</em>' attribute.
   * @see #setRoles(boolean)
   * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent_Roles()
   * @model
   * @generated
   */
  boolean isRoles();

  /**
   * Sets the value of the '{@link org.xtext.example.rsec.rSec.Event#isRoles <em>Roles</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Roles</em>' attribute.
   * @see #isRoles()
   * @generated
   */
  void setRoles(boolean value);

  /**
   * Returns the value of the '<em><b>Mrole</b></em>' reference list.
   * The list contents are of type {@link org.xtext.example.rsec.rSec.Roles}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mrole</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mrole</em>' reference list.
   * @see org.xtext.example.rsec.rSec.RSecPackage#getEvent_Mrole()
   * @model
   * @generated
   */
  EList<Roles> getMrole();

} // Event

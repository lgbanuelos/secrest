package org.xtext.example.rsec.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.rsec.services.RSecGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRSecParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'package'", "'datatype'", "'import'", "'.*'", "'.'", "'role'", "'resource'", "'on'", "'view'", "'data'", "'end'", "'actions'", "'{'", "'->'", "','", "'}'", "'states'", "'('", "')'", "':'", "'many'", "'with'", "'roles'", "'['", "']'", "'rendered'", "'state'", "'=>'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__38=38;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRSecParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRSecParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRSecParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRSec.g"; }



     	private RSecGrammarAccess grammarAccess;

        public InternalRSecParser(TokenStream input, RSecGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "ResourceSpecification";
       	}

       	@Override
       	protected RSecGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleResourceSpecification"
    // InternalRSec.g:64:1: entryRuleResourceSpecification returns [EObject current=null] : iv_ruleResourceSpecification= ruleResourceSpecification EOF ;
    public final EObject entryRuleResourceSpecification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResourceSpecification = null;


        try {
            // InternalRSec.g:64:62: (iv_ruleResourceSpecification= ruleResourceSpecification EOF )
            // InternalRSec.g:65:2: iv_ruleResourceSpecification= ruleResourceSpecification EOF
            {
             newCompositeNode(grammarAccess.getResourceSpecificationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResourceSpecification=ruleResourceSpecification();

            state._fsp--;

             current =iv_ruleResourceSpecification; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResourceSpecification"


    // $ANTLR start "ruleResourceSpecification"
    // InternalRSec.g:71:1: ruleResourceSpecification returns [EObject current=null] : ( () otherlv_1= 'package' ( (lv_packageName_2_0= ruleQualifiedName ) ) ( (lv_imports_3_0= ruleImport ) )* ( (lv_elements_4_0= ruleType ) )* ) ;
    public final EObject ruleResourceSpecification() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_packageName_2_0 = null;

        EObject lv_imports_3_0 = null;

        EObject lv_elements_4_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:77:2: ( ( () otherlv_1= 'package' ( (lv_packageName_2_0= ruleQualifiedName ) ) ( (lv_imports_3_0= ruleImport ) )* ( (lv_elements_4_0= ruleType ) )* ) )
            // InternalRSec.g:78:2: ( () otherlv_1= 'package' ( (lv_packageName_2_0= ruleQualifiedName ) ) ( (lv_imports_3_0= ruleImport ) )* ( (lv_elements_4_0= ruleType ) )* )
            {
            // InternalRSec.g:78:2: ( () otherlv_1= 'package' ( (lv_packageName_2_0= ruleQualifiedName ) ) ( (lv_imports_3_0= ruleImport ) )* ( (lv_elements_4_0= ruleType ) )* )
            // InternalRSec.g:79:3: () otherlv_1= 'package' ( (lv_packageName_2_0= ruleQualifiedName ) ) ( (lv_imports_3_0= ruleImport ) )* ( (lv_elements_4_0= ruleType ) )*
            {
            // InternalRSec.g:79:3: ()
            // InternalRSec.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getResourceSpecificationAccess().getResourceSpecificationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getResourceSpecificationAccess().getPackageKeyword_1());
            		
            // InternalRSec.g:90:3: ( (lv_packageName_2_0= ruleQualifiedName ) )
            // InternalRSec.g:91:4: (lv_packageName_2_0= ruleQualifiedName )
            {
            // InternalRSec.g:91:4: (lv_packageName_2_0= ruleQualifiedName )
            // InternalRSec.g:92:5: lv_packageName_2_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getResourceSpecificationAccess().getPackageNameQualifiedNameParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_packageName_2_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getResourceSpecificationRule());
            					}
            					set(
            						current,
            						"packageName",
            						lv_packageName_2_0,
            						"org.xtext.example.rsec.RSec.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRSec.g:109:3: ( (lv_imports_3_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==13) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalRSec.g:110:4: (lv_imports_3_0= ruleImport )
            	    {
            	    // InternalRSec.g:110:4: (lv_imports_3_0= ruleImport )
            	    // InternalRSec.g:111:5: lv_imports_3_0= ruleImport
            	    {

            	    					newCompositeNode(grammarAccess.getResourceSpecificationAccess().getImportsImportParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_imports_3_0=ruleImport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getResourceSpecificationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_3_0,
            	    						"org.xtext.example.rsec.RSec.Import");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalRSec.g:128:3: ( (lv_elements_4_0= ruleType ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12||(LA2_0>=16 && LA2_0<=17)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalRSec.g:129:4: (lv_elements_4_0= ruleType )
            	    {
            	    // InternalRSec.g:129:4: (lv_elements_4_0= ruleType )
            	    // InternalRSec.g:130:5: lv_elements_4_0= ruleType
            	    {

            	    					newCompositeNode(grammarAccess.getResourceSpecificationAccess().getElementsTypeParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_elements_4_0=ruleType();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getResourceSpecificationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"elements",
            	    						lv_elements_4_0,
            	    						"org.xtext.example.rsec.RSec.Type");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResourceSpecification"


    // $ANTLR start "entryRuleType"
    // InternalRSec.g:151:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalRSec.g:151:45: (iv_ruleType= ruleType EOF )
            // InternalRSec.g:152:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalRSec.g:158:1: ruleType returns [EObject current=null] : (this_DataType_0= ruleDataType | this_ResourceType_1= ruleResourceType | this_Roles_2= ruleRoles ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_DataType_0 = null;

        EObject this_ResourceType_1 = null;

        EObject this_Roles_2 = null;



        	enterRule();

        try {
            // InternalRSec.g:164:2: ( (this_DataType_0= ruleDataType | this_ResourceType_1= ruleResourceType | this_Roles_2= ruleRoles ) )
            // InternalRSec.g:165:2: (this_DataType_0= ruleDataType | this_ResourceType_1= ruleResourceType | this_Roles_2= ruleRoles )
            {
            // InternalRSec.g:165:2: (this_DataType_0= ruleDataType | this_ResourceType_1= ruleResourceType | this_Roles_2= ruleRoles )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt3=1;
                }
                break;
            case 17:
                {
                alt3=2;
                }
                break;
            case 16:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalRSec.g:166:3: this_DataType_0= ruleDataType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getDataTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_DataType_0=ruleDataType();

                    state._fsp--;


                    			current = this_DataType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalRSec.g:175:3: this_ResourceType_1= ruleResourceType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getResourceTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ResourceType_1=ruleResourceType();

                    state._fsp--;


                    			current = this_ResourceType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalRSec.g:184:3: this_Roles_2= ruleRoles
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getRolesParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Roles_2=ruleRoles();

                    state._fsp--;


                    			current = this_Roles_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleDataType"
    // InternalRSec.g:196:1: entryRuleDataType returns [EObject current=null] : iv_ruleDataType= ruleDataType EOF ;
    public final EObject entryRuleDataType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataType = null;


        try {
            // InternalRSec.g:196:49: (iv_ruleDataType= ruleDataType EOF )
            // InternalRSec.g:197:2: iv_ruleDataType= ruleDataType EOF
            {
             newCompositeNode(grammarAccess.getDataTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataType=ruleDataType();

            state._fsp--;

             current =iv_ruleDataType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataType"


    // $ANTLR start "ruleDataType"
    // InternalRSec.g:203:1: ruleDataType returns [EObject current=null] : (otherlv_0= 'datatype' ( (lv_name_1_0= ruleQualifiedName ) ) ) ;
    public final EObject ruleDataType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:209:2: ( (otherlv_0= 'datatype' ( (lv_name_1_0= ruleQualifiedName ) ) ) )
            // InternalRSec.g:210:2: (otherlv_0= 'datatype' ( (lv_name_1_0= ruleQualifiedName ) ) )
            {
            // InternalRSec.g:210:2: (otherlv_0= 'datatype' ( (lv_name_1_0= ruleQualifiedName ) ) )
            // InternalRSec.g:211:3: otherlv_0= 'datatype' ( (lv_name_1_0= ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getDataTypeAccess().getDatatypeKeyword_0());
            		
            // InternalRSec.g:215:3: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalRSec.g:216:4: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalRSec.g:216:4: (lv_name_1_0= ruleQualifiedName )
            // InternalRSec.g:217:5: lv_name_1_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getDataTypeAccess().getNameQualifiedNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.xtext.example.rsec.RSec.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataType"


    // $ANTLR start "entryRuleImport"
    // InternalRSec.g:238:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalRSec.g:238:47: (iv_ruleImport= ruleImport EOF )
            // InternalRSec.g:239:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalRSec.g:245:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:251:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) )
            // InternalRSec.g:252:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            {
            // InternalRSec.g:252:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            // InternalRSec.g:253:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalRSec.g:257:3: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalRSec.g:258:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalRSec.g:258:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // InternalRSec.g:259:5: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportRule());
            					}
            					set(
            						current,
            						"importedNamespace",
            						lv_importedNamespace_1_0,
            						"org.xtext.example.rsec.RSec.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalRSec.g:280:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalRSec.g:280:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalRSec.g:281:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalRSec.g:287:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:293:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalRSec.g:294:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalRSec.g:294:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalRSec.g:295:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {

            			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_6);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalRSec.g:305:3: (kw= '.*' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalRSec.g:306:4: kw= '.*'
                    {
                    kw=(Token)match(input,14,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalRSec.g:316:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalRSec.g:316:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalRSec.g:317:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalRSec.g:323:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalRSec.g:329:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalRSec.g:330:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalRSec.g:330:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalRSec.g:331:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalRSec.g:338:3: (kw= '.' this_ID_2= RULE_ID )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==15) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalRSec.g:339:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,15,FOLLOW_3); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_7); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleRoles"
    // InternalRSec.g:356:1: entryRuleRoles returns [EObject current=null] : iv_ruleRoles= ruleRoles EOF ;
    public final EObject entryRuleRoles() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoles = null;


        try {
            // InternalRSec.g:356:46: (iv_ruleRoles= ruleRoles EOF )
            // InternalRSec.g:357:2: iv_ruleRoles= ruleRoles EOF
            {
             newCompositeNode(grammarAccess.getRolesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRoles=ruleRoles();

            state._fsp--;

             current =iv_ruleRoles; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoles"


    // $ANTLR start "ruleRoles"
    // InternalRSec.g:363:1: ruleRoles returns [EObject current=null] : (otherlv_0= 'role' ( (lv_name_1_0= ruleQualifiedName ) ) ) ;
    public final EObject ruleRoles() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:369:2: ( (otherlv_0= 'role' ( (lv_name_1_0= ruleQualifiedName ) ) ) )
            // InternalRSec.g:370:2: (otherlv_0= 'role' ( (lv_name_1_0= ruleQualifiedName ) ) )
            {
            // InternalRSec.g:370:2: (otherlv_0= 'role' ( (lv_name_1_0= ruleQualifiedName ) ) )
            // InternalRSec.g:371:3: otherlv_0= 'role' ( (lv_name_1_0= ruleQualifiedName ) )
            {
            otherlv_0=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getRolesAccess().getRoleKeyword_0());
            		
            // InternalRSec.g:375:3: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalRSec.g:376:4: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalRSec.g:376:4: (lv_name_1_0= ruleQualifiedName )
            // InternalRSec.g:377:5: lv_name_1_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getRolesAccess().getNameQualifiedNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRolesRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.xtext.example.rsec.RSec.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoles"


    // $ANTLR start "entryRuleResourceType"
    // InternalRSec.g:398:1: entryRuleResourceType returns [EObject current=null] : iv_ruleResourceType= ruleResourceType EOF ;
    public final EObject entryRuleResourceType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResourceType = null;


        try {
            // InternalRSec.g:398:53: (iv_ruleResourceType= ruleResourceType EOF )
            // InternalRSec.g:399:2: iv_ruleResourceType= ruleResourceType EOF
            {
             newCompositeNode(grammarAccess.getResourceTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResourceType=ruleResourceType();

            state._fsp--;

             current =iv_ruleResourceType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResourceType"


    // $ANTLR start "ruleResourceType"
    // InternalRSec.g:405:1: ruleResourceType returns [EObject current=null] : (otherlv_0= 'resource' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'on' ( (lv_path_3_0= RULE_STRING ) ) )? (otherlv_4= 'view' ( (lv_views_5_0= RULE_STRING ) ) )? (otherlv_6= 'data' ( (lv_properties_7_0= ruleProperty ) )+ otherlv_8= 'end' )? (otherlv_9= 'actions' (otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}' )? ( (lv_events_19_0= ruleEvent ) )+ otherlv_20= 'end' )? (otherlv_21= 'states' ( (lv_states_22_0= ruleState ) )* otherlv_23= 'end' )? otherlv_24= 'end' ) ;
    public final EObject ruleResourceType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token lv_path_3_0=null;
        Token otherlv_4=null;
        Token lv_views_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token lv_pathvariable_11_0=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token lv_pathvariable_15_0=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_properties_7_0 = null;

        EObject lv_events_19_0 = null;

        EObject lv_states_22_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:411:2: ( (otherlv_0= 'resource' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'on' ( (lv_path_3_0= RULE_STRING ) ) )? (otherlv_4= 'view' ( (lv_views_5_0= RULE_STRING ) ) )? (otherlv_6= 'data' ( (lv_properties_7_0= ruleProperty ) )+ otherlv_8= 'end' )? (otherlv_9= 'actions' (otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}' )? ( (lv_events_19_0= ruleEvent ) )+ otherlv_20= 'end' )? (otherlv_21= 'states' ( (lv_states_22_0= ruleState ) )* otherlv_23= 'end' )? otherlv_24= 'end' ) )
            // InternalRSec.g:412:2: (otherlv_0= 'resource' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'on' ( (lv_path_3_0= RULE_STRING ) ) )? (otherlv_4= 'view' ( (lv_views_5_0= RULE_STRING ) ) )? (otherlv_6= 'data' ( (lv_properties_7_0= ruleProperty ) )+ otherlv_8= 'end' )? (otherlv_9= 'actions' (otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}' )? ( (lv_events_19_0= ruleEvent ) )+ otherlv_20= 'end' )? (otherlv_21= 'states' ( (lv_states_22_0= ruleState ) )* otherlv_23= 'end' )? otherlv_24= 'end' )
            {
            // InternalRSec.g:412:2: (otherlv_0= 'resource' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'on' ( (lv_path_3_0= RULE_STRING ) ) )? (otherlv_4= 'view' ( (lv_views_5_0= RULE_STRING ) ) )? (otherlv_6= 'data' ( (lv_properties_7_0= ruleProperty ) )+ otherlv_8= 'end' )? (otherlv_9= 'actions' (otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}' )? ( (lv_events_19_0= ruleEvent ) )+ otherlv_20= 'end' )? (otherlv_21= 'states' ( (lv_states_22_0= ruleState ) )* otherlv_23= 'end' )? otherlv_24= 'end' )
            // InternalRSec.g:413:3: otherlv_0= 'resource' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'on' ( (lv_path_3_0= RULE_STRING ) ) )? (otherlv_4= 'view' ( (lv_views_5_0= RULE_STRING ) ) )? (otherlv_6= 'data' ( (lv_properties_7_0= ruleProperty ) )+ otherlv_8= 'end' )? (otherlv_9= 'actions' (otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}' )? ( (lv_events_19_0= ruleEvent ) )+ otherlv_20= 'end' )? (otherlv_21= 'states' ( (lv_states_22_0= ruleState ) )* otherlv_23= 'end' )? otherlv_24= 'end'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getResourceTypeAccess().getResourceKeyword_0());
            		
            // InternalRSec.g:417:3: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalRSec.g:418:4: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalRSec.g:418:4: (lv_name_1_0= ruleQualifiedName )
            // InternalRSec.g:419:5: lv_name_1_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getResourceTypeAccess().getNameQualifiedNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_8);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getResourceTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.xtext.example.rsec.RSec.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRSec.g:436:3: (otherlv_2= 'on' ( (lv_path_3_0= RULE_STRING ) ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalRSec.g:437:4: otherlv_2= 'on' ( (lv_path_3_0= RULE_STRING ) )
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_9); 

                    				newLeafNode(otherlv_2, grammarAccess.getResourceTypeAccess().getOnKeyword_2_0());
                    			
                    // InternalRSec.g:441:4: ( (lv_path_3_0= RULE_STRING ) )
                    // InternalRSec.g:442:5: (lv_path_3_0= RULE_STRING )
                    {
                    // InternalRSec.g:442:5: (lv_path_3_0= RULE_STRING )
                    // InternalRSec.g:443:6: lv_path_3_0= RULE_STRING
                    {
                    lv_path_3_0=(Token)match(input,RULE_STRING,FOLLOW_10); 

                    						newLeafNode(lv_path_3_0, grammarAccess.getResourceTypeAccess().getPathSTRINGTerminalRuleCall_2_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getResourceTypeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"path",
                    							lv_path_3_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalRSec.g:460:3: (otherlv_4= 'view' ( (lv_views_5_0= RULE_STRING ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==19) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalRSec.g:461:4: otherlv_4= 'view' ( (lv_views_5_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,19,FOLLOW_9); 

                    				newLeafNode(otherlv_4, grammarAccess.getResourceTypeAccess().getViewKeyword_3_0());
                    			
                    // InternalRSec.g:465:4: ( (lv_views_5_0= RULE_STRING ) )
                    // InternalRSec.g:466:5: (lv_views_5_0= RULE_STRING )
                    {
                    // InternalRSec.g:466:5: (lv_views_5_0= RULE_STRING )
                    // InternalRSec.g:467:6: lv_views_5_0= RULE_STRING
                    {
                    lv_views_5_0=(Token)match(input,RULE_STRING,FOLLOW_11); 

                    						newLeafNode(lv_views_5_0, grammarAccess.getResourceTypeAccess().getViewsSTRINGTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getResourceTypeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"views",
                    							lv_views_5_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalRSec.g:484:3: (otherlv_6= 'data' ( (lv_properties_7_0= ruleProperty ) )+ otherlv_8= 'end' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==20) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalRSec.g:485:4: otherlv_6= 'data' ( (lv_properties_7_0= ruleProperty ) )+ otherlv_8= 'end'
                    {
                    otherlv_6=(Token)match(input,20,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getResourceTypeAccess().getDataKeyword_4_0());
                    			
                    // InternalRSec.g:489:4: ( (lv_properties_7_0= ruleProperty ) )+
                    int cnt8=0;
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==RULE_ID) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalRSec.g:490:5: (lv_properties_7_0= ruleProperty )
                    	    {
                    	    // InternalRSec.g:490:5: (lv_properties_7_0= ruleProperty )
                    	    // InternalRSec.g:491:6: lv_properties_7_0= ruleProperty
                    	    {

                    	    						newCompositeNode(grammarAccess.getResourceTypeAccess().getPropertiesPropertyParserRuleCall_4_1_0());
                    	    					
                    	    pushFollow(FOLLOW_12);
                    	    lv_properties_7_0=ruleProperty();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getResourceTypeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"properties",
                    	    							lv_properties_7_0,
                    	    							"org.xtext.example.rsec.RSec.Property");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt8 >= 1 ) break loop8;
                                EarlyExitException eee =
                                    new EarlyExitException(8, input);
                                throw eee;
                        }
                        cnt8++;
                    } while (true);

                    otherlv_8=(Token)match(input,21,FOLLOW_13); 

                    				newLeafNode(otherlv_8, grammarAccess.getResourceTypeAccess().getEndKeyword_4_2());
                    			

                    }
                    break;

            }

            // InternalRSec.g:513:3: (otherlv_9= 'actions' (otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}' )? ( (lv_events_19_0= ruleEvent ) )+ otherlv_20= 'end' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==22) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalRSec.g:514:4: otherlv_9= 'actions' (otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}' )? ( (lv_events_19_0= ruleEvent ) )+ otherlv_20= 'end'
                    {
                    otherlv_9=(Token)match(input,22,FOLLOW_14); 

                    				newLeafNode(otherlv_9, grammarAccess.getResourceTypeAccess().getActionsKeyword_5_0());
                    			
                    // InternalRSec.g:518:4: (otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0==23) ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // InternalRSec.g:519:5: otherlv_10= '{' ( (lv_pathvariable_11_0= RULE_STRING ) ) otherlv_12= '->' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )* otherlv_18= '}'
                            {
                            otherlv_10=(Token)match(input,23,FOLLOW_9); 

                            					newLeafNode(otherlv_10, grammarAccess.getResourceTypeAccess().getLeftCurlyBracketKeyword_5_1_0());
                            				
                            // InternalRSec.g:523:5: ( (lv_pathvariable_11_0= RULE_STRING ) )
                            // InternalRSec.g:524:6: (lv_pathvariable_11_0= RULE_STRING )
                            {
                            // InternalRSec.g:524:6: (lv_pathvariable_11_0= RULE_STRING )
                            // InternalRSec.g:525:7: lv_pathvariable_11_0= RULE_STRING
                            {
                            lv_pathvariable_11_0=(Token)match(input,RULE_STRING,FOLLOW_15); 

                            							newLeafNode(lv_pathvariable_11_0, grammarAccess.getResourceTypeAccess().getPathvariableSTRINGTerminalRuleCall_5_1_1_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getResourceTypeRule());
                            							}
                            							addWithLastConsumed(
                            								current,
                            								"pathvariable",
                            								lv_pathvariable_11_0,
                            								"org.eclipse.xtext.common.Terminals.STRING");
                            						

                            }


                            }

                            otherlv_12=(Token)match(input,24,FOLLOW_3); 

                            					newLeafNode(otherlv_12, grammarAccess.getResourceTypeAccess().getHyphenMinusGreaterThanSignKeyword_5_1_2());
                            				
                            // InternalRSec.g:545:5: ( (otherlv_13= RULE_ID ) )
                            // InternalRSec.g:546:6: (otherlv_13= RULE_ID )
                            {
                            // InternalRSec.g:546:6: (otherlv_13= RULE_ID )
                            // InternalRSec.g:547:7: otherlv_13= RULE_ID
                            {

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getResourceTypeRule());
                            							}
                            						
                            otherlv_13=(Token)match(input,RULE_ID,FOLLOW_16); 

                            							newLeafNode(otherlv_13, grammarAccess.getResourceTypeAccess().getResourceResourceTypeCrossReference_5_1_3_0());
                            						

                            }


                            }

                            // InternalRSec.g:558:5: (otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) ) )*
                            loop10:
                            do {
                                int alt10=2;
                                int LA10_0 = input.LA(1);

                                if ( (LA10_0==25) ) {
                                    alt10=1;
                                }


                                switch (alt10) {
                            	case 1 :
                            	    // InternalRSec.g:559:6: otherlv_14= ',' ( (lv_pathvariable_15_0= RULE_STRING ) ) otherlv_16= '->' ( (otherlv_17= RULE_ID ) )
                            	    {
                            	    otherlv_14=(Token)match(input,25,FOLLOW_9); 

                            	    						newLeafNode(otherlv_14, grammarAccess.getResourceTypeAccess().getCommaKeyword_5_1_4_0());
                            	    					
                            	    // InternalRSec.g:563:6: ( (lv_pathvariable_15_0= RULE_STRING ) )
                            	    // InternalRSec.g:564:7: (lv_pathvariable_15_0= RULE_STRING )
                            	    {
                            	    // InternalRSec.g:564:7: (lv_pathvariable_15_0= RULE_STRING )
                            	    // InternalRSec.g:565:8: lv_pathvariable_15_0= RULE_STRING
                            	    {
                            	    lv_pathvariable_15_0=(Token)match(input,RULE_STRING,FOLLOW_15); 

                            	    								newLeafNode(lv_pathvariable_15_0, grammarAccess.getResourceTypeAccess().getPathvariableSTRINGTerminalRuleCall_5_1_4_1_0());
                            	    							

                            	    								if (current==null) {
                            	    									current = createModelElement(grammarAccess.getResourceTypeRule());
                            	    								}
                            	    								addWithLastConsumed(
                            	    									current,
                            	    									"pathvariable",
                            	    									lv_pathvariable_15_0,
                            	    									"org.eclipse.xtext.common.Terminals.STRING");
                            	    							

                            	    }


                            	    }

                            	    otherlv_16=(Token)match(input,24,FOLLOW_3); 

                            	    						newLeafNode(otherlv_16, grammarAccess.getResourceTypeAccess().getHyphenMinusGreaterThanSignKeyword_5_1_4_2());
                            	    					
                            	    // InternalRSec.g:585:6: ( (otherlv_17= RULE_ID ) )
                            	    // InternalRSec.g:586:7: (otherlv_17= RULE_ID )
                            	    {
                            	    // InternalRSec.g:586:7: (otherlv_17= RULE_ID )
                            	    // InternalRSec.g:587:8: otherlv_17= RULE_ID
                            	    {

                            	    								if (current==null) {
                            	    									current = createModelElement(grammarAccess.getResourceTypeRule());
                            	    								}
                            	    							
                            	    otherlv_17=(Token)match(input,RULE_ID,FOLLOW_16); 

                            	    								newLeafNode(otherlv_17, grammarAccess.getResourceTypeAccess().getResourceResourceTypeCrossReference_5_1_4_3_0());
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop10;
                                }
                            } while (true);

                            otherlv_18=(Token)match(input,26,FOLLOW_14); 

                            					newLeafNode(otherlv_18, grammarAccess.getResourceTypeAccess().getRightCurlyBracketKeyword_5_1_5());
                            				

                            }
                            break;

                    }

                    // InternalRSec.g:604:4: ( (lv_events_19_0= ruleEvent ) )+
                    int cnt12=0;
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==RULE_ID) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalRSec.g:605:5: (lv_events_19_0= ruleEvent )
                    	    {
                    	    // InternalRSec.g:605:5: (lv_events_19_0= ruleEvent )
                    	    // InternalRSec.g:606:6: lv_events_19_0= ruleEvent
                    	    {

                    	    						newCompositeNode(grammarAccess.getResourceTypeAccess().getEventsEventParserRuleCall_5_2_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_events_19_0=ruleEvent();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getResourceTypeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"events",
                    	    							lv_events_19_0,
                    	    							"org.xtext.example.rsec.RSec.Event");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt12 >= 1 ) break loop12;
                                EarlyExitException eee =
                                    new EarlyExitException(12, input);
                                throw eee;
                        }
                        cnt12++;
                    } while (true);

                    otherlv_20=(Token)match(input,21,FOLLOW_18); 

                    				newLeafNode(otherlv_20, grammarAccess.getResourceTypeAccess().getEndKeyword_5_3());
                    			

                    }
                    break;

            }

            // InternalRSec.g:628:3: (otherlv_21= 'states' ( (lv_states_22_0= ruleState ) )* otherlv_23= 'end' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==27) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalRSec.g:629:4: otherlv_21= 'states' ( (lv_states_22_0= ruleState ) )* otherlv_23= 'end'
                    {
                    otherlv_21=(Token)match(input,27,FOLLOW_19); 

                    				newLeafNode(otherlv_21, grammarAccess.getResourceTypeAccess().getStatesKeyword_6_0());
                    			
                    // InternalRSec.g:633:4: ( (lv_states_22_0= ruleState ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==37) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalRSec.g:634:5: (lv_states_22_0= ruleState )
                    	    {
                    	    // InternalRSec.g:634:5: (lv_states_22_0= ruleState )
                    	    // InternalRSec.g:635:6: lv_states_22_0= ruleState
                    	    {

                    	    						newCompositeNode(grammarAccess.getResourceTypeAccess().getStatesStateParserRuleCall_6_1_0());
                    	    					
                    	    pushFollow(FOLLOW_19);
                    	    lv_states_22_0=ruleState();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getResourceTypeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"states",
                    	    							lv_states_22_0,
                    	    							"org.xtext.example.rsec.RSec.State");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    otherlv_23=(Token)match(input,21,FOLLOW_20); 

                    				newLeafNode(otherlv_23, grammarAccess.getResourceTypeAccess().getEndKeyword_6_2());
                    			

                    }
                    break;

            }

            otherlv_24=(Token)match(input,21,FOLLOW_2); 

            			newLeafNode(otherlv_24, grammarAccess.getResourceTypeAccess().getEndKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResourceType"


    // $ANTLR start "entryRuleEvent"
    // InternalRSec.g:665:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // InternalRSec.g:665:46: (iv_ruleEvent= ruleEvent EOF )
            // InternalRSec.g:666:2: iv_ruleEvent= ruleEvent EOF
            {
             newCompositeNode(grammarAccess.getEventRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEvent=ruleEvent();

            state._fsp--;

             current =iv_ruleEvent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalRSec.g:672:1: ruleEvent returns [EObject current=null] : ( ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= '(' ( (otherlv_2= RULE_ID ) )? otherlv_3= ')' otherlv_4= ':' ( (lv_many_5_0= 'many' ) )? ( (otherlv_6= RULE_ID ) ) otherlv_7= 'with' ( (lv_verb_8_0= RULE_ID ) ) otherlv_9= 'on' ( (lv_path_10_0= RULE_STRING ) ) ( (lv_roles_11_0= 'roles' ) )? (otherlv_12= '[' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= ']' )? ) ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_many_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token lv_verb_8_0=null;
        Token otherlv_9=null;
        Token lv_path_10_0=null;
        Token lv_roles_11_0=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:678:2: ( ( ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= '(' ( (otherlv_2= RULE_ID ) )? otherlv_3= ')' otherlv_4= ':' ( (lv_many_5_0= 'many' ) )? ( (otherlv_6= RULE_ID ) ) otherlv_7= 'with' ( (lv_verb_8_0= RULE_ID ) ) otherlv_9= 'on' ( (lv_path_10_0= RULE_STRING ) ) ( (lv_roles_11_0= 'roles' ) )? (otherlv_12= '[' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= ']' )? ) )
            // InternalRSec.g:679:2: ( ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= '(' ( (otherlv_2= RULE_ID ) )? otherlv_3= ')' otherlv_4= ':' ( (lv_many_5_0= 'many' ) )? ( (otherlv_6= RULE_ID ) ) otherlv_7= 'with' ( (lv_verb_8_0= RULE_ID ) ) otherlv_9= 'on' ( (lv_path_10_0= RULE_STRING ) ) ( (lv_roles_11_0= 'roles' ) )? (otherlv_12= '[' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= ']' )? )
            {
            // InternalRSec.g:679:2: ( ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= '(' ( (otherlv_2= RULE_ID ) )? otherlv_3= ')' otherlv_4= ':' ( (lv_many_5_0= 'many' ) )? ( (otherlv_6= RULE_ID ) ) otherlv_7= 'with' ( (lv_verb_8_0= RULE_ID ) ) otherlv_9= 'on' ( (lv_path_10_0= RULE_STRING ) ) ( (lv_roles_11_0= 'roles' ) )? (otherlv_12= '[' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= ']' )? )
            // InternalRSec.g:680:3: ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= '(' ( (otherlv_2= RULE_ID ) )? otherlv_3= ')' otherlv_4= ':' ( (lv_many_5_0= 'many' ) )? ( (otherlv_6= RULE_ID ) ) otherlv_7= 'with' ( (lv_verb_8_0= RULE_ID ) ) otherlv_9= 'on' ( (lv_path_10_0= RULE_STRING ) ) ( (lv_roles_11_0= 'roles' ) )? (otherlv_12= '[' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= ']' )?
            {
            // InternalRSec.g:680:3: ( (lv_name_0_0= ruleQualifiedName ) )
            // InternalRSec.g:681:4: (lv_name_0_0= ruleQualifiedName )
            {
            // InternalRSec.g:681:4: (lv_name_0_0= ruleQualifiedName )
            // InternalRSec.g:682:5: lv_name_0_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getEventAccess().getNameQualifiedNameParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_21);
            lv_name_0_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEventRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.xtext.example.rsec.RSec.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,28,FOLLOW_22); 

            			newLeafNode(otherlv_1, grammarAccess.getEventAccess().getLeftParenthesisKeyword_1());
            		
            // InternalRSec.g:703:3: ( (otherlv_2= RULE_ID ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_ID) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalRSec.g:704:4: (otherlv_2= RULE_ID )
                    {
                    // InternalRSec.g:704:4: (otherlv_2= RULE_ID )
                    // InternalRSec.g:705:5: otherlv_2= RULE_ID
                    {

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getEventRule());
                    					}
                    				
                    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_23); 

                    					newLeafNode(otherlv_2, grammarAccess.getEventAccess().getParamTypeResourceTypeCrossReference_2_0());
                    				

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,29,FOLLOW_24); 

            			newLeafNode(otherlv_3, grammarAccess.getEventAccess().getRightParenthesisKeyword_3());
            		
            otherlv_4=(Token)match(input,30,FOLLOW_25); 

            			newLeafNode(otherlv_4, grammarAccess.getEventAccess().getColonKeyword_4());
            		
            // InternalRSec.g:724:3: ( (lv_many_5_0= 'many' ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==31) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalRSec.g:725:4: (lv_many_5_0= 'many' )
                    {
                    // InternalRSec.g:725:4: (lv_many_5_0= 'many' )
                    // InternalRSec.g:726:5: lv_many_5_0= 'many'
                    {
                    lv_many_5_0=(Token)match(input,31,FOLLOW_3); 

                    					newLeafNode(lv_many_5_0, grammarAccess.getEventAccess().getManyManyKeyword_5_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getEventRule());
                    					}
                    					setWithLastConsumed(current, "many", true, "many");
                    				

                    }


                    }
                    break;

            }

            // InternalRSec.g:738:3: ( (otherlv_6= RULE_ID ) )
            // InternalRSec.g:739:4: (otherlv_6= RULE_ID )
            {
            // InternalRSec.g:739:4: (otherlv_6= RULE_ID )
            // InternalRSec.g:740:5: otherlv_6= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEventRule());
            					}
            				
            otherlv_6=(Token)match(input,RULE_ID,FOLLOW_26); 

            					newLeafNode(otherlv_6, grammarAccess.getEventAccess().getReturnTypeTypeCrossReference_6_0());
            				

            }


            }

            otherlv_7=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_7, grammarAccess.getEventAccess().getWithKeyword_7());
            		
            // InternalRSec.g:755:3: ( (lv_verb_8_0= RULE_ID ) )
            // InternalRSec.g:756:4: (lv_verb_8_0= RULE_ID )
            {
            // InternalRSec.g:756:4: (lv_verb_8_0= RULE_ID )
            // InternalRSec.g:757:5: lv_verb_8_0= RULE_ID
            {
            lv_verb_8_0=(Token)match(input,RULE_ID,FOLLOW_27); 

            					newLeafNode(lv_verb_8_0, grammarAccess.getEventAccess().getVerbIDTerminalRuleCall_8_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEventRule());
            					}
            					setWithLastConsumed(
            						current,
            						"verb",
            						lv_verb_8_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_9=(Token)match(input,18,FOLLOW_9); 

            			newLeafNode(otherlv_9, grammarAccess.getEventAccess().getOnKeyword_9());
            		
            // InternalRSec.g:777:3: ( (lv_path_10_0= RULE_STRING ) )
            // InternalRSec.g:778:4: (lv_path_10_0= RULE_STRING )
            {
            // InternalRSec.g:778:4: (lv_path_10_0= RULE_STRING )
            // InternalRSec.g:779:5: lv_path_10_0= RULE_STRING
            {
            lv_path_10_0=(Token)match(input,RULE_STRING,FOLLOW_28); 

            					newLeafNode(lv_path_10_0, grammarAccess.getEventAccess().getPathSTRINGTerminalRuleCall_10_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEventRule());
            					}
            					setWithLastConsumed(
            						current,
            						"path",
            						lv_path_10_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalRSec.g:795:3: ( (lv_roles_11_0= 'roles' ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==33) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalRSec.g:796:4: (lv_roles_11_0= 'roles' )
                    {
                    // InternalRSec.g:796:4: (lv_roles_11_0= 'roles' )
                    // InternalRSec.g:797:5: lv_roles_11_0= 'roles'
                    {
                    lv_roles_11_0=(Token)match(input,33,FOLLOW_29); 

                    					newLeafNode(lv_roles_11_0, grammarAccess.getEventAccess().getRolesRolesKeyword_11_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getEventRule());
                    					}
                    					setWithLastConsumed(current, "roles", true, "roles");
                    				

                    }


                    }
                    break;

            }

            // InternalRSec.g:809:3: (otherlv_12= '[' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= ']' )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==34) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalRSec.g:810:4: otherlv_12= '[' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* otherlv_16= ']'
                    {
                    otherlv_12=(Token)match(input,34,FOLLOW_3); 

                    				newLeafNode(otherlv_12, grammarAccess.getEventAccess().getLeftSquareBracketKeyword_12_0());
                    			
                    // InternalRSec.g:814:4: ( (otherlv_13= RULE_ID ) )
                    // InternalRSec.g:815:5: (otherlv_13= RULE_ID )
                    {
                    // InternalRSec.g:815:5: (otherlv_13= RULE_ID )
                    // InternalRSec.g:816:6: otherlv_13= RULE_ID
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getEventRule());
                    						}
                    					
                    otherlv_13=(Token)match(input,RULE_ID,FOLLOW_30); 

                    						newLeafNode(otherlv_13, grammarAccess.getEventAccess().getMroleRolesCrossReference_12_1_0());
                    					

                    }


                    }

                    // InternalRSec.g:827:4: (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==25) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // InternalRSec.g:828:5: otherlv_14= ',' ( (otherlv_15= RULE_ID ) )
                    	    {
                    	    otherlv_14=(Token)match(input,25,FOLLOW_3); 

                    	    					newLeafNode(otherlv_14, grammarAccess.getEventAccess().getCommaKeyword_12_2_0());
                    	    				
                    	    // InternalRSec.g:832:5: ( (otherlv_15= RULE_ID ) )
                    	    // InternalRSec.g:833:6: (otherlv_15= RULE_ID )
                    	    {
                    	    // InternalRSec.g:833:6: (otherlv_15= RULE_ID )
                    	    // InternalRSec.g:834:7: otherlv_15= RULE_ID
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getEventRule());
                    	    							}
                    	    						
                    	    otherlv_15=(Token)match(input,RULE_ID,FOLLOW_30); 

                    	    							newLeafNode(otherlv_15, grammarAccess.getEventAccess().getMroleRolesCrossReference_12_2_1_0());
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,35,FOLLOW_2); 

                    				newLeafNode(otherlv_16, grammarAccess.getEventAccess().getRightSquareBracketKeyword_12_3());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleProperty"
    // InternalRSec.g:855:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // InternalRSec.g:855:49: (iv_ruleProperty= ruleProperty EOF )
            // InternalRSec.g:856:2: iv_ruleProperty= ruleProperty EOF
            {
             newCompositeNode(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProperty=ruleProperty();

            state._fsp--;

             current =iv_ruleProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalRSec.g:862:1: ruleProperty returns [EObject current=null] : ( ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= ':' ( (lv_many_2_0= 'many' ) )? ( (otherlv_3= RULE_ID ) ) ( (lv_render_4_0= 'rendered' ) )? ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_many_2_0=null;
        Token otherlv_3=null;
        Token lv_render_4_0=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:868:2: ( ( ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= ':' ( (lv_many_2_0= 'many' ) )? ( (otherlv_3= RULE_ID ) ) ( (lv_render_4_0= 'rendered' ) )? ) )
            // InternalRSec.g:869:2: ( ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= ':' ( (lv_many_2_0= 'many' ) )? ( (otherlv_3= RULE_ID ) ) ( (lv_render_4_0= 'rendered' ) )? )
            {
            // InternalRSec.g:869:2: ( ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= ':' ( (lv_many_2_0= 'many' ) )? ( (otherlv_3= RULE_ID ) ) ( (lv_render_4_0= 'rendered' ) )? )
            // InternalRSec.g:870:3: ( (lv_name_0_0= ruleQualifiedName ) ) otherlv_1= ':' ( (lv_many_2_0= 'many' ) )? ( (otherlv_3= RULE_ID ) ) ( (lv_render_4_0= 'rendered' ) )?
            {
            // InternalRSec.g:870:3: ( (lv_name_0_0= ruleQualifiedName ) )
            // InternalRSec.g:871:4: (lv_name_0_0= ruleQualifiedName )
            {
            // InternalRSec.g:871:4: (lv_name_0_0= ruleQualifiedName )
            // InternalRSec.g:872:5: lv_name_0_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getPropertyAccess().getNameQualifiedNameParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_24);
            lv_name_0_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPropertyRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.xtext.example.rsec.RSec.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,30,FOLLOW_25); 

            			newLeafNode(otherlv_1, grammarAccess.getPropertyAccess().getColonKeyword_1());
            		
            // InternalRSec.g:893:3: ( (lv_many_2_0= 'many' ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==31) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalRSec.g:894:4: (lv_many_2_0= 'many' )
                    {
                    // InternalRSec.g:894:4: (lv_many_2_0= 'many' )
                    // InternalRSec.g:895:5: lv_many_2_0= 'many'
                    {
                    lv_many_2_0=(Token)match(input,31,FOLLOW_3); 

                    					newLeafNode(lv_many_2_0, grammarAccess.getPropertyAccess().getManyManyKeyword_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPropertyRule());
                    					}
                    					setWithLastConsumed(current, "many", true, "many");
                    				

                    }


                    }
                    break;

            }

            // InternalRSec.g:907:3: ( (otherlv_3= RULE_ID ) )
            // InternalRSec.g:908:4: (otherlv_3= RULE_ID )
            {
            // InternalRSec.g:908:4: (otherlv_3= RULE_ID )
            // InternalRSec.g:909:5: otherlv_3= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getPropertyRule());
            					}
            				
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_31); 

            					newLeafNode(otherlv_3, grammarAccess.getPropertyAccess().getTypeTypeCrossReference_3_0());
            				

            }


            }

            // InternalRSec.g:920:3: ( (lv_render_4_0= 'rendered' ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==36) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalRSec.g:921:4: (lv_render_4_0= 'rendered' )
                    {
                    // InternalRSec.g:921:4: (lv_render_4_0= 'rendered' )
                    // InternalRSec.g:922:5: lv_render_4_0= 'rendered'
                    {
                    lv_render_4_0=(Token)match(input,36,FOLLOW_2); 

                    					newLeafNode(lv_render_4_0, grammarAccess.getPropertyAccess().getRenderRenderedKeyword_4_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPropertyRule());
                    					}
                    					setWithLastConsumed(current, "render", true, "rendered");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleState"
    // InternalRSec.g:938:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalRSec.g:938:46: (iv_ruleState= ruleState EOF )
            // InternalRSec.g:939:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalRSec.g:945:1: ruleState returns [EObject current=null] : (otherlv_0= 'state' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_transitions_2_0= ruleTransition ) )* otherlv_3= 'end' ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_transitions_2_0 = null;



        	enterRule();

        try {
            // InternalRSec.g:951:2: ( (otherlv_0= 'state' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_transitions_2_0= ruleTransition ) )* otherlv_3= 'end' ) )
            // InternalRSec.g:952:2: (otherlv_0= 'state' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_transitions_2_0= ruleTransition ) )* otherlv_3= 'end' )
            {
            // InternalRSec.g:952:2: (otherlv_0= 'state' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_transitions_2_0= ruleTransition ) )* otherlv_3= 'end' )
            // InternalRSec.g:953:3: otherlv_0= 'state' ( (lv_name_1_0= ruleQualifiedName ) ) ( (lv_transitions_2_0= ruleTransition ) )* otherlv_3= 'end'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getStateAccess().getStateKeyword_0());
            		
            // InternalRSec.g:957:3: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalRSec.g:958:4: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalRSec.g:958:4: (lv_name_1_0= ruleQualifiedName )
            // InternalRSec.g:959:5: lv_name_1_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getStateAccess().getNameQualifiedNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_12);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStateRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.xtext.example.rsec.RSec.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalRSec.g:976:3: ( (lv_transitions_2_0= ruleTransition ) )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==RULE_ID) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalRSec.g:977:4: (lv_transitions_2_0= ruleTransition )
            	    {
            	    // InternalRSec.g:977:4: (lv_transitions_2_0= ruleTransition )
            	    // InternalRSec.g:978:5: lv_transitions_2_0= ruleTransition
            	    {

            	    					newCompositeNode(grammarAccess.getStateAccess().getTransitionsTransitionParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_transitions_2_0=ruleTransition();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStateRule());
            	    					}
            	    					add(
            	    						current,
            	    						"transitions",
            	    						lv_transitions_2_0,
            	    						"org.xtext.example.rsec.RSec.Transition");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            otherlv_3=(Token)match(input,21,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getStateAccess().getEndKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRuleTransition"
    // InternalRSec.g:1003:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalRSec.g:1003:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalRSec.g:1004:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalRSec.g:1010:1: ruleTransition returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalRSec.g:1016:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) ) ) )
            // InternalRSec.g:1017:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) ) )
            {
            // InternalRSec.g:1017:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) ) )
            // InternalRSec.g:1018:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '=>' ( (otherlv_2= RULE_ID ) )
            {
            // InternalRSec.g:1018:3: ( (otherlv_0= RULE_ID ) )
            // InternalRSec.g:1019:4: (otherlv_0= RULE_ID )
            {
            // InternalRSec.g:1019:4: (otherlv_0= RULE_ID )
            // InternalRSec.g:1020:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_32); 

            					newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getEventEventCrossReference_0_0());
            				

            }


            }

            otherlv_1=(Token)match(input,38,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getEqualsSignGreaterThanSignKeyword_1());
            		
            // InternalRSec.g:1035:3: ( (otherlv_2= RULE_ID ) )
            // InternalRSec.g:1036:4: (otherlv_2= RULE_ID )
            {
            // InternalRSec.g:1036:4: (otherlv_2= RULE_ID )
            // InternalRSec.g:1037:5: otherlv_2= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getStateStateCrossReference_2_0());
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000033002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000031002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000000087C0000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000008780000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000008700000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000200010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000008600000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000800010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000006000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000A00010L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000008200000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000002000200000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000020000010L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000080000010L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000600000002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000802000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000001000000002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000004000000000L});

}